<?php

return [

    'name'        => 'Infinity Business',
    'title'       => 'Escritório, Escritórios Virtuais, Coworking e Salas de Reunião – Infinity Business',
    'description' => 'Espaço de coworking, escritório virtual e salas de reunião em Alphaville e região.',
    'keywords'    => 'Espaço de escritórios, escritórios para locação, escritórios corporativos, escritórios virtuais, endereço físico, encaminhamento de correspondência, salas de reunião, salas de conferência, coworking, escritórios compartilhados, coworking alphaville',
    'share_image' => '',
    'analytics'   => 'UA-75996362-1'

];
