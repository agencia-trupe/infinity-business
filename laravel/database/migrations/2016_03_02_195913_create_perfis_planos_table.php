<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfisPlanosTable extends Migration
{
    public function up()
    {
        Schema::create('perfis_planos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('perfil_id')->unsigned();
            $table->integer('plano_id')->unsigned();
            $table->timestamps();
            $table->foreign('perfil_id')->references('id')->on('perfis');
            $table->foreign('plano_id')->references('id')->on('planos');
        });
    }

    public function down()
    {
        Schema::drop('perfis_planos');
    }
}
