<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('indicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('indicado');
            $table->string('por');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('indicacoes');
    }
}
