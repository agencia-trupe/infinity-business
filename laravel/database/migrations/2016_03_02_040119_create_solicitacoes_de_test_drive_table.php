<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitacoesDeTestDriveTable extends Migration
{
    public function up()
    {
        Schema::create('solicitacoes_de_test_drive', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->boolean('estacao_de_trabalho')->default(0);
            $table->boolean('sala_de_reuniao')->default(0);
            $table->string('nome');
            $table->string('e_mail');
            $table->string('telefone');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('solicitacoes_de_test_drive');
    }
}
