<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComparativoTable extends Migration
{
    public function up()
    {
        Schema::create('comparativo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->text('comparativo');
            $table->text('comparativo_en');
            $table->text('comparativo_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('comparativo');
    }
}
