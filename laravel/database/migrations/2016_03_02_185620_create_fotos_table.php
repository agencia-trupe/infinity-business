<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosTable extends Migration
{
    public function up()
    {
        Schema::create('fotos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('legenda')->nullable();
            $table->string('galeria');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('fotos');
    }
}
