<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanosTable extends Migration
{
    public function up()
    {
        Schema::create('planos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('nome_en');
            $table->string('nome_es');
            $table->string('slug');
            $table->string('sigla');
            $table->integer('tipo');
            $table->string('valor');
            $table->text('chamada');
            $table->text('chamada_en');
            $table->text('chamada_es');
            $table->text('texto');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('planos');
    }
}
