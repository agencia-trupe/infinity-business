<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComparativoPlanosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comparativo_planos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comparativo_id')->unsigned();
            $table->integer('plano_id')->unsigned();
            $table->timestamps();
            $table->foreign('comparativo_id')->references('id')->on('comparativo');
            $table->foreign('plano_id')->references('id')->on('planos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comparativo_planos');
    }
}
