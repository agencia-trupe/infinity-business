<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMidiaTable extends Migration
{
    public function up()
    {
        Schema::create('midia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('data');
            $table->string('veiculo');
            $table->string('titulo');
            $table->string('imagem');
            $table->string('pdf');
            $table->string('link');
            $table->timestamps();
        });

        Schema::create('midia_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('midia_id')->unsigned();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->timestamps();
            $table->foreign('midia_id')->references('id')->on('midia')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('midia_imagens');
        Schema::drop('midia');
    }
}
