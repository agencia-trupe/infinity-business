<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTable extends Migration
{
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_1');
            $table->text('texto_1_en');
            $table->text('texto_1_es');
            $table->text('texto_2');
            $table->text('texto_2_en');
            $table->text('texto_2_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('facilities');
    }
}
