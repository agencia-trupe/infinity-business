<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('video_tipo');
            $table->string('video_codigo');
            $table->string('video_codigo_en');
            $table->string('video_codigo_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('video');
    }
}
