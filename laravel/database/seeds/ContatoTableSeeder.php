<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'             => 'contato@infinitybusiness.com',
            'telefone'          => '11 3736·1800',
            'endereco'          => '<p>Alameda Rio Negro, 503 &middot; 23&ordm; andar</p><p>Edif&iacute;cio Rio Negro</p><p>Alphaville &middot; Barueri, SP</p><p>06454-000</p>',
            'codigo_googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.8695065199336!2d-46.850324285023504!3d-23.50120928471281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf0223bf7d7ea3%3A0x5c69e5300371b547!2sAlameda+Rio+Negro%2C+503+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1457456321376" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
            'endereco_en' => ''
        ]);
    }
}
