<?php

use Illuminate\Database\Seeder;

class PlanosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planos')->insert([
            [
                'nome'    => 'Premium Business Plan',
                'nome_en' => 'Premium Business Plan',
                'nome_es' => 'Premium Business Plan',
                'slug'    => 'premium-business-plan',
                'sigla'   => 'PBP',
                'tipo'    => 1,
                'valor'   => '1.500,00',
                'chamada' => 'Escritórios prontos exclusivos, equipados com móveis de alto padrão, tecnologia de ponta e serviços personalizados. Contratos de locação flexíveis e sem investimento inicial.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Shared Business Plan',
                'nome_en' => 'Shared Business Plan',
                'nome_es' => 'Shared Business Plan',
                'slug'    => 'shared-business-plan',
                'sigla'   => 'SBP',
                'tipo'    => 1,
                'valor'   => '1.250,00',
                'chamada' => 'Estações de trabalho privativas completas em ambiente executivo compartilhado com acesso 24 horas / 7 dias na semana.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Coworking Business Plan',
                'nome_en' => 'Coworking Business Plan',
                'nome_es' => 'Coworking Business Plan',
                'slug'    => 'coworking-business-plan',
                'sigla'   => 'CBP',
                'tipo'    => 1,
                'valor'   => '1.000,00',
                'chamada' => 'Estações de trabalho rotativas em ambiente executivo compartilhado com acesso livre em dias úteis, das 9h às 18h.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Virtual Business Plan',
                'nome_en' => 'Virtual Business Plan',
                'nome_es' => 'Virtual Business Plan',
                'slug'    => 'virtual-business-plan',
                'sigla'   => 'VBP',
                'tipo'    => 1,
                'valor'   => '400,00',
                'chamada' => 'Endereço empresarial privilegiado com linha telefônica exclusiva e atendimento bilíngue personalizado. Escritórios e salas de reuniões à disposição sob demanda. Acesso livre das 9h às 18h em dias úteis.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Digital Signage Plan',
                'nome_en' => 'Digital Signage Plan',
                'nome_es' => 'Digital Signage Plan',
                'slug'    => 'digital-signage-plan',
                'sigla'   => '',
                'tipo'    => 2,
                'valor'   => '',
                'chamada' => 'Produção e divulgação de vídeos e animações corporativas personalizadas reproduzidas para rede de network da Infinity Business e nos monitores e videowalls do centro de negócio. INVESTIMENTOS SOB CONSULTA.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Networking',
                'nome_en' => 'Networking',
                'nome_es' => 'Networking',
                'slug'    => 'networking',
                'sigla'   => '',
                'tipo'    => 2,
                'valor'   => '',
                'chamada' => 'Participação na rede de networking da Infinity, através de rede de relacionamento privada on-line, eventos periódicos com conteúdo de qualidade e happy-hours que objetivam ampliar a rede de relacionamento profissional dos participantes.',
                'chamada_en' => ''
            ],
            [
                'nome'    => 'Eventos e Reuniões',
                'nome_en' => 'Events and Meetings',
                'nome_es' => 'Eventos y Reuniones',
                'slug'    => 'eventos-e-reunioes',
                'sigla'   => '',
                'tipo'    => 2,
                'valor'   => '',
                'chamada' => 'Modernas e equipadas salas de reuniões, auditórios e espaços para conferências, showrooms, workshops e treinamentos com serviços de coffee-break e videoconferência.',
                'chamada_en' => ''
            ],
        ]);
    }
}
