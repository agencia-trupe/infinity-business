<!DOCTYPE html>
<html>
<head>
    <title>[SOLICITAÇÃO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Data:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $data }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $e_mail }}</span><br>
    @if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Estação de Trabalho:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $estacao_de_trabalho ? 'Sim' : 'Não' }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Sala de Reunião:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $sala_de_reuniao ? 'Sim' : 'Não' }}</span><br>
</body>
</html>
