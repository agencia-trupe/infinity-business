<!DOCTYPE html>
<html>
<head>
    <title>[INDICAÇÃO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    Você foi indicado pelo e-mail <em>{{ $por }}</em> para conhecer a <a href="http://www.infinitybusiness.com.br">Infinity Business</a>, um novo conceito de Business Center no Brasil!
</body>
</html>