<!DOCTYPE html>
<html>
<head>
    <title>[INDICAÇÃO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    O e-mail <em>{{ $indicado }}</em> foi indicado por <em>{{ $por }}</em> para conhecer a Infinity Business.
</body>
</html>
