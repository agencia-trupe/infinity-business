@extends('frontend.common.template', [
    'cleanPage' => true
])

@section('content')

    <section id="not-found">
        <h3>PÁGINA NÃO ENCONTRADA</h3>
        <a href="/">VOLTAR PARA A HOME</a>
    </section>

@endsection
