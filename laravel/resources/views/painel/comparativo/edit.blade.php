@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comparativo /</small> Editar Comparativo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.comparativo.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.comparativo.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
