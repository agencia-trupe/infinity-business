@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('comparativo', 'Comparativo') !!}
        {!! Form::text('comparativo', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('comparativo_en', 'Comparativo (INGLÊS)') !!}
        {!! Form::text('comparativo_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('comparativo_es', 'Comparativo (ESPANHOL)') !!}
        {!! Form::text('comparativo_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row form-group">
    @foreach($planos as $plano)
    <div class="col-md-3">
        <div class="checkbox">
            <label>
                {!! Form::checkbox('planos[]', $plano->id, (isset($registro) ? ($registro->planos->contains($plano->id) ? true : false) : false)) !!}
                {{ $plano->nome }}
            </label>
        </div>
    </div>
    @endforeach
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.comparativo.index') }}" class="btn btn-default btn-voltar">Voltar</a>
