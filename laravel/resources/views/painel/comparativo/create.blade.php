@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comparativo /</small> Adicionar Comparativo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.comparativo.store', 'files' => true]) !!}

        @include('painel.comparativo.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
