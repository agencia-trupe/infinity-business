@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Facilities</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.facilities.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.facilities.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
