@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_1', 'Texto 1') !!}
        {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_1_en', 'Texto 1 (INGLÊS)') !!}
        {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_1_es', 'Texto 1 (ESPANHOL)') !!}
        {!! Form::textarea('texto_1_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto_2', 'Texto 2') !!}
        {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_2_en', 'Texto 2 (INGLÊS)') !!}
        {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_2_es', 'Texto 2 (ESPANHOL)') !!}
        {!! Form::textarea('texto_2_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
