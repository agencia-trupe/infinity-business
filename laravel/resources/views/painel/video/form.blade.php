@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/video/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; width: 400px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('video_tipo', 'Vídeo - Tipo') !!}
    {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('video_codigo', 'Vídeo - Código') !!}
        {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('video_codigo_en', 'Vídeo - Código (INGLÊS)') !!}
        {!! Form::text('video_codigo_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('video_codigo_es', 'Vídeo - Código (ESPANHOL)') !!}
        {!! Form::text('video_codigo_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
