@include('painel.common.flash')

@unless(isset($registro) && $registro->tipo === 2)
<div class="form-group">
    {!! Form::label('valor', 'Valor') !!}
    {!! Form::text('valor', null, ['class' => 'form-control']) !!}
</div>
@endif

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('chamada', 'Chamada') !!}
        {!! Form::textarea('chamada', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('chamada_en', 'Chamada (INGLÊS)') !!}
        {!! Form::textarea('chamada_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('chamada_es', 'Chamada (ESPANHOL)') !!}
        {!! Form::textarea('chamada_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_en', 'Texto (INGLÊS)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_es', 'Texto (ESPANHOL)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.planos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
