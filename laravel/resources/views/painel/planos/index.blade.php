@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Planos
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Nome (INGLÊS)</th>
                <th>Nome (ESPANHOL)</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->nome_en }}</td>
                <td>{{ $registro->nome_es }}</td>
                <td class="crud-actions">
                    <a href="{{ route('painel.planos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
