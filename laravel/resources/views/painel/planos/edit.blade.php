@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Planos /</small> {{ $registro->nome }}</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.planos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.planos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
