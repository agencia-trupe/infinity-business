@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Fotos /</small> {{ ucfirst($galeria) }}

            @unless($hideCreateBtn == true)
            {!! Form::open(['route' => ['painel.fotos.store', $galeria], 'files' => true, 'class' => 'pull-right']) !!}
                <span class="btn btn-success btn-sm" style="position:relative;overflow:hidden">
                    <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>
                    Adicionar Fotos
                    <input id="images-upload" type="file" name="imagem" id="imagem" multiple style="position:absolute;top:0;right:0;opacity:0;font-size:200px;cursor:pointer;">
                </span>
            {!! Form::close() !!}
            @endif
        </h2>
    </legend>

    <div class="progress progress-striped active">
        <div class="progress-bar" style="width: 0"></div>
    </div>

    <div class="alert alert-block alert-danger errors" style="display:none"></div>

    <div class="alert alert-info" style="display:inline-block;padding:10px 15px;">
        <small>
            <span class="glyphicon glyphicon-move" style="margin-right: 10px;"></span>
            Clique e arraste as fotos para ordená-las.
        </small>
    </div>

    <div id="imagens" data-table="fotos">
    @if(!count($imagens))
        <div class="alert alert-warning no-images" role="alert">Nenhuma foto cadastrada.</div>
    @else
        @foreach($imagens as $imagem)
        @include('painel.fotos.imagem')
        @endforeach
    @endif
    </div>

@endsection
