<div class="imagem imagem-edit-legenda col-md-3 col-sm-3 col-xs-4" style="position:relative;padding:0;border:1px solid #efefef;margin-left:-1px;margin-top:-1px;" id="{{ $imagem->id }}" data-ordem="{{ $imagem->ordem or 0 }}" data-imagem="{{ $imagem->imagem }}">
    <img src="{{ url('assets/img/fotos/thumbs/'.$imagem->imagem) }}" alt="" style="display:block;width:100%;height:auto;cursor:move;">
    {!! Form::open([
        'route'  => ['painel.fotos.destroy', $imagem->id],
        'method' => 'delete'
    ]) !!}

    <div class="btn-group btn-group-sm" style="position:absolute;bottom:8px;left:10px;">
        <a href="javascript:void(0)" data-href="{{ route('painel.fotos.show', $imagem->id) }}" class="btn btn-primary btn-sm imagem-edit-legenda" data-path="{{ url('assets/img/fotos/thumbs/') }}"><span class="glyphicon glyphicon-pencil"></span></a>
        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-trash"></span></button>
    </div>

    {!! Form::close() !!}
</div>
