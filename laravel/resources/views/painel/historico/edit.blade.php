@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Histórico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.historico.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.historico.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
