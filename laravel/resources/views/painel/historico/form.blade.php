@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_en', 'Texto (INGLÊS)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_es', 'Texto (ESPANHOL)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
