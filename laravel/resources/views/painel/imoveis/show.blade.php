@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Imóveis Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $contato->e_mail }}</div>
    </div>

@if($contato->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Localização do Imóvel</label>
        <div class="well">{{ $contato->localizacao }}</div>
    </div>

    <a href="{{ route('painel.imoveis.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
