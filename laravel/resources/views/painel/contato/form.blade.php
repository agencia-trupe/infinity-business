@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('endereco', 'Endereço') !!}
        {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('endereco_en', 'Endereço (INGLÊS)') !!}
        {!! Form::textarea('endereco_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('endereco_es', 'Endereço (ESPANHOL)') !!}
        {!! Form::textarea('endereco_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('codigo_googlemaps', 'Código GoogleMaps') !!}
    {!! Form::text('codigo_googlemaps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
