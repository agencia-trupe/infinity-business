@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::text('data', null, ['class' => 'form-control monthpicker']) !!}
</div>

<div class="form-group">
    {!! Form::label('veiculo', 'Veículo') !!}
    {!! Form::text('veiculo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (opcional)') !!}
@if($submitText == 'Alterar' && $registro->imagem)
    <img src="{{ url('assets/img/midia/thumbs/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    <a href="{{ route('painel.midia.deleteImage', $registro->id) }}" class="btn-delete btn-delete-link label label-danger" style="display:inline-block;margin-bottom:10px">
        <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
        REMOVER
    </a>
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('pdf', 'PDF (opcional, se preenchido precede a galeria de imagens)') !!}
    @if($submitText == 'Alterar' && $registro->pdf)
    <p>
        <a href="{{ url('assets/pdfs/'.$registro->pdf) }}" target="_blank" style="display:block;">{{ $registro->pdf }}</a>
        <a href="{{ route('painel.midia.deletePdf', $registro->id) }}" class="btn-delete btn-delete-link label label-danger">
            <i class="glyphicon glyphicon-remove" style="margin-right:5px"></i>
            REMOVER
        </a>
    </p>
    @endif
    {!! Form::file('pdf', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link externo (opcional, se preenchido precede a galeria de imagens e PDF)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.midia.index') }}" class="btn btn-default btn-voltar">Voltar</a>
