@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfis /</small> Editar Perfil</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.perfis.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.perfis.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
