@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Perfis /</small> Adicionar Perfil</h2>
    </legend>

    {!! Form::open(['route' => 'painel.perfis.store', 'files' => true]) !!}

        @include('painel.perfis.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
