@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('nome', 'Nome') !!}
        {!! Form::text('nome', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('nome_en', 'Nome (INGLÊS)') !!}
        {!! Form::text('nome_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('nome_es', 'Nome (ESPANHOL)') !!}
        {!! Form::text('nome_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_en', 'Texto (INGLÊS)') !!}
        {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('texto_es', 'Texto (ESPANHOL)') !!}
        {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'planos']) !!}
    </div>
</div>

<div class="row form-group">
    @foreach($planos as $plano)
    <div class="col-md-3">
        <div class="checkbox">
            <label>
                {!! Form::checkbox('planos[]', $plano->id, (isset($registro) ? ($registro->planos->contains($plano->id) ? true : false) : false)) !!}
                {{ $plano->nome }}
            </label>
        </div>
    </div>
    @endforeach
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.perfis.index') }}" class="btn btn-default btn-voltar">Voltar</a>
