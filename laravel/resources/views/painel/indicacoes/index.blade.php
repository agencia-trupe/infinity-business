@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Indicações
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data</th>
                <th>Cliente Indicado</th>
                <th>Indicado Por</th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->created_at }}</td>
                <td>{{ $registro->indicado }}</td>
                <td>{{ $registro->por }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->render() }}

    @endif

@endsection
