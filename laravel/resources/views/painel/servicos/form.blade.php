@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('titulo', 'Título') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_en', 'Título (INGLÊS)') !!}
        {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-4">
        {!! Form::label('titulo_es', 'Título (ESPANHOL)') !!}
        {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.servicos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
