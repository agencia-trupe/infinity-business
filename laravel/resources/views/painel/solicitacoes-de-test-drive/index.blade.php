@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Solicitações de Test-Drive
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Estação de Trabalho</th>
                <th>Sala de Reunião</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row @if(!$registro->lido) warning @endif">
                <td>{{ $registro->data }}</td>
                <td>{{ $registro->nome }}</td>
                <td>{{ $registro->e_mail }}</td>
                <td>{{ $registro->telefone }}</td>
                <td>
                    @if($registro->estacao_de_trabalho)
                    <span class="glyphicon glyphicon-ok"></span>
                    @endif
                </td>
                <td>
                    @if($registro->sala_de_reuniao)
                    <span class="glyphicon glyphicon-ok"></span>
                    @endif
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.solicitacoes-de-test-drive.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}
                        <div class="btn-group">
                            @if($registro->lido)
                                <a href="{{ route('painel.solicitacoes-de-test-drive.show', $registro->id) }}" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-repeat" style="margin-right:10px"></span>Não lido</a>
                            @else
                                <a href="{{ route('painel.solicitacoes-de-test-drive.show', $registro->id) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-ok" style="margin-right:10px"></span>Lido</a>
                            @endif
                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $registros->render() }}

    @endif

@endsection
