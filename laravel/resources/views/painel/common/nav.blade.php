<ul class="nav navbar-nav">
    <li @if(str_is('painel.video*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.video.index') }}">Vídeo</a>
    </li>
    <li @if(str_is('painel.facilities*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.facilities.index') }}">Facilities</a>
    </li>
    <li @if(str_is('painel.perfis*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.perfis.index') }}">Perfis</a>
    </li>
    <li @if(str_is('painel.planos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.planos.index') }}">Planos</a>
    </li>
    <li @if(str_is('painel.comparativo*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.comparativo.index') }}">Comparativo</a>
    </li>
    <li @if(str_is('painel.servicos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.servicos.index') }}">Serviços</a>
    </li>
    <li @if(str_is('painel.historico*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.historico.index') }}">Histórico</a>
    </li>
    <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
	<li @if(str_is('painel.midia*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.midia.index') }}">Mídia</a>
	</li>
    <li class="dropdown @if(str_is('painel.fotos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Fotos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.fotos.index', 'conceito') }}">Conceito</a></li>
            <li><a href="{{ route('painel.fotos.index', 'experimente') }}">Experimente</a></li>
            <li><a href="{{ route('painel.fotos.index', 'contato') }}">Contato</a></li>
        </ul>
    </li>
    <li @if(str_is('painel.solicitacoes-de-test-drive*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.solicitacoes-de-test-drive.index') }}">
            Test-Drive
            @if($solicitacoesNaoLidas >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $solicitacoesNaoLidas }}</span>
            @endif
        </a>
    </li>
    <li @if(str_is('painel.indicacoes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.indicacoes.index') }}">Indicações</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.imoveis*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.imoveis.index') }}">
            Imóveis
            @if($imoveisNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $imoveisNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
