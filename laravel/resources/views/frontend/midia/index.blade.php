<div class="lightbox-wrapper midia-content">
    <h1>{{ trans('frontend.header.midia') }}</h1>

    <div class="midia-thumbs">
    @foreach($midia as $midia)
        @if($midia->link || $midia->pdf)
            @if($midia->imagem)
                <a href="{{ $midia->link ?: asset('assets/pdfs/'.$midia->pdf) }}" target="_blank">
                    <div class="quadrado">
                        <img src="{{ asset('assets/img/midia/thumbs/'.$midia->imagem) }}" alt="{{ $midia->titulo }}">
                        <div class="overlay overlay-link"></div>
                    </div>
                    <p>
                        {{ $midia->veiculo }}<br>
                        {{ Tools::formataData($midia->data, app()->getLocale()) }}
                    </p>
                </a>
            @else
                <a href="{{ $midia->link ?: asset('assets/pdfs/'.$midia->pdf) }}" target="_blank">
                    <div class="quadrado">
                        <span>{{ $midia->titulo }}</span>
                        <div class="overlay overlay-link"></div>
                    </div>
                    <p>
                        {{ $midia->veiculo }}<br>
                        {{ Tools::formataData($midia->data, app()->getLocale()) }}
                    </p>
                </a>
            @endif
        @else
            @if($midia->imagem)
                <a href="{{ route('midia.show', $midia->id) }}" class="lightbox-content">
                    <div class="quadrado">
                        <img src="{{ asset('assets/img/midia/thumbs/'.$midia->imagem) }}" alt="{{ $midia->titulo }}">
                        <div class="overlay"></div>
                    </div>
                    <p>
                        {{ $midia->veiculo }}<br>
                        {{ Tools::formataData($midia->data, app()->getLocale()) }}
                    </p>
                </a>
            @else
                <a href="{{ route('midia.show', $midia->id) }}" class="lightbox-content">
                    <div class="quadrado">
                        <span>{{ $midia->titulo }}</span>
                        <div class="overlay"></div>
                    </div>
                    <p>
                        {{ $midia->veiculo }}<br>
                        {{ Tools::formataData($midia->data, app()->getLocale()) }}
                    </p>
                </a>
            @endif
        @endif
    @endforeach
    </div>
</div>
