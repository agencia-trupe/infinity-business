<div class="lightbox-wrapper midia-content">
    <h1>{{ trans('frontend.header.midia') }}</h1>

    <div class="midia-show">
        <div class="midia-slider">
            @foreach($midia->imagens as $imagem)
            <div>
                <img src="{{ asset('assets/img/midia/imagens/'.$imagem->imagem) }}" alt="">
            </div>
            @endforeach
            <a href="#" class="controls cycle-prev">prev</a>
            <a href="#" class="controls cycle-next">next</a>
        </div>

        <p>
            {{ $midia->veiculo }}
            &middot;
            {{ Tools::formataData($midia->data, app()->getLocale()) }}
        </p>

        <a href="{{ route('midia') }}" class="lightbox-content midia-voltar">
            <span></span>
            {{ trans('frontend.voltar') }}
        </a>
    </div>
</div>

<script>
    $('.midia-slider').cycle({
        slides: '>div',
        timeout: 0
    });
</script>
