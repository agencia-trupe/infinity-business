@extends('frontend.common.template', [
    'cleanPage' => true
])

@section('content')

    <section id="not-found">
        <h3>{{ mb_strtoupper(trans('frontend.indique.sucesso')) }}</h3>
        <a href="{{ route('home') }}">
            {{ trans('frontend.voltar-home') }}
        </a>
    </section>

    <!-- Google Code for Convers&otilde;es Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 875868630;
    var google_conversion_label = "bxVdCNrflWsQ1uPSoQM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/875868630/?label=bxVdCNrflWsQ1uPSoQM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

@endsection
