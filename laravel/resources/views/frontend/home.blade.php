@extends('frontend.common.template')

@section('content')

    <section id="video" style="background-image: url('{{ asset('assets/img/video/'.$video->imagem) }}')">
        <div class="center">
            <div class="video-box">
                @if($video->video_tipo == 'youtube')
                <iframe src="http://www.youtube.com/embed/{{ $video->{trans('banco.video_codigo')} }}" width="600" height" 400" class="video-player" frameborder="0" allowfullscreen></iframe>
                @elseif($video->video_tipo == 'vimeo')
                <iframe src="https://player.vimeo.com/video/{{ $video->{trans('banco.video_codigo')} }}" class="video-player" frameborder="0" allowfullscreen></iframe>
                @endif
            </div>

            <div class="botoes">
                <a href="#" class="video-handle">
                    <span>
                        {{ trans('frontend.video.titulo') }}
                    </span>
                    <img src="{{ asset('assets/img/layout/simbolo-play-home.png') }}" alt="">
                </a>
                <a href="contato" class="nav-scroll contrate">
                    <span>{{ trans('frontend.video.contrate') }}</span>
                </a>
            </div>
        </div>

        <a href="conceito" class="nav-scroll saiba-mais">{{ trans('frontend.video.saiba-mais') }}</a>
    </section>

    <section id="conceito">
        <div class="center">
            <h2>{{ trans('frontend.conceito.titulo') }}</h2>

            <div>
                <div class="conceito-col">
                    <img src="{{ asset('assets/img/layout/img-escritorios-prontos.png') }}" alt="">
                    <h3>{{ trans('frontend.conceito.escritorios') }}</h3>
                    <p>{{ trans('frontend.conceito.escritorios-sub') }}</p>
                </div>
                <div class="conceito-col">
                    <img src="{{ asset('assets/img/layout/img-identidade-empresarial.png') }}" alt="">
                    <h3>{{ trans('frontend.conceito.identidade') }}</h3>
                    <p>{{ trans('frontend.conceito.identidade-sub') }}</p>
                </div>
                <div class="conceito-col">
                    <img src="{{ asset('assets/img/layout/img-eventos-negocios.png') }}" alt="">
                    <h3>{{ trans('frontend.conceito.eventos') }}</h3>
                    <p>{{ trans('frontend.conceito.eventos-sub') }}</p>
                </div>
                <div class="conceito-col">
                    <img src="{{ asset('assets/img/layout/img-facilidades-flexibilidade.png') }}" alt="">
                    <h3>{{ trans('frontend.conceito.facilidades') }}</h3>
                    <p>{{ trans('frontend.conceito.facilidades-sub') }}</p>
                </div>
            </div>

            <div class="conceito-galeria">
                @foreach($fotos['conceito'] as $foto)
                <a href="{{ asset('assets/img/fotos/'.$foto->imagem) }}" rel="galeria-conceito" title="{{ $foto->legenda }}" class="lightbox-image">
                    <img src="{{ asset('assets/img/fotos/thumbs/'.$foto->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </section>

    <section id="facilities">
        <div class="center">
            <h2>{{ trans('frontend.facilities.titulo') }}</h2>

            <div class="facilities-texto">
                <div class="col">
                    {!! $facilities->{trans('banco.texto_1')} !!}
                </div>
                <div class="col">
                    {!! $facilities->{trans('banco.texto_2')} !!}
                </div>
            </div>
        </div>

        <a href="#" class="facilities-handle">
            {{ trans('frontend.facilities.saiba-mais') }}
        </a>
    </section>

    <section id="perfis">
        <div class="center">
            <h2>{{ trans('frontend.perfis.titulo') }}</h2>

            <div class="perfis-lista">
                @foreach($perfis->chunk(3) as $chunk)
                <div class="row">
                    @foreach($chunk as $perfil)
                    <a href="{{ route('perfil', $perfil->slug) }}" class="lightbox-content">{{ $perfil->{trans('banco.nome')} }}</a>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="planos">
        <div class="center">
            <h2>{{ trans('frontend.planos.titulo') }}</h2>

            <div class="planos-tipo-1">
                @foreach($planos->where('tipo', 1) as $plano)
                <a href="{{ route('plano', $plano->slug) }}" class="lightbox-content">
                    <div class="icone">
                        <img src="{{ asset('assets/img/layout/icone-'.$plano->sigla.'.png') }}" alt="">
                    </div>
                    <div class="box">
                        <div class="detalhe"></div>
                        <h3><span>{{ $plano->sigla }}</span> {{ $plano->{trans('banco.nome')} }}</h3>
                        <p class="chamada">{{ $plano->{trans('banco.chamada')} }}</p>
                        <p class="preco">
                            {{ trans('frontend.planos.a-partir') }}<br>
                            <span>R$ {{ $plano->valor }}</span> /{{ trans('frontend.planos.mes') }}
                            @if($plano->sigla !== 'VBP')
                            <br>{{ trans('frontend.planos.por-estacao') }}
                            @endif
                        </p>
                        <div class="mais">
                            {{ trans('frontend.planos.saber-mais') }}
                            <img src="{{ asset('assets/img/layout/simbolo-mais-saber-mais.png') }}" alt="">
                        </div>
                    </div>
                </a>
                @endforeach
            </div>

            <div class="planos-tipo-2">
                @foreach($planos->where('tipo', 2) as $plano)
                <a href="{{ route('plano', $plano->slug) }}" class="{{ $plano->slug }} lightbox-content">
                    <div class="left">
                        <div class="left-wrapper">
                            <h3>{{ $plano->{trans('banco.nome')} }}</h3>
                        </div>
                    </div>
                    <div class="chamada">
                        <p>{{ $plano->{trans('banco.chamada')} }}</p>
                    </div>
                    <div class="right">
                        <div class="right-wrapper">
                            <p>
                                {!! trans('frontend.planos.saber-mais-2') !!}
                                <img src="{{ asset('assets/img/layout/simbolo-mais-saber-mais.png') }}" alt="">
                            </p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </section>

    <section id="compare">
        <div class="center">
            <h2>{{ trans('frontend.compare.titulo') }}</h2>

            <table class="compare-tabela">
                <col class="coluna-principal">
                <col class="coluna-checkbox">
                <col class="coluna-checkbox">
                <col class="coluna-checkbox">
                <thead>
                    <tr>
                        <th class="titulo">{{ trans('frontend.compare.tabela-titulo') }}</th>
                        @foreach($planos->where('tipo', 1) as $plano)
                        <th>
                            <img src="{{ asset('assets/img/layout/icone-tabela-'.$plano->sigla.'.png') }}" alt="">
                            {{ $plano->sigla }}
                        </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($comparativo as $linha)
                    <tr>
                        <td class="titulo">{{ $linha->{trans('banco.comparativo')} }}</td>
                        @foreach($planos->where('tipo', 1) as $plano)
                        <td>
                            @if($linha->planos->contains($plano->id))
                            <img src="{{ asset('assets/img/layout/simbolo-checked.png') }}" alt="">
                            @endif
                        </td>
                        @endforeach
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <p class="legenda">{{ trans('frontend.compare.tabela-legenda') }}</p>
            <a href="http://infinityspaces.com.br/" target="_blank" class="infinity-spaces">
                {{ trans('frontend.compare.infinity-spaces') }} &raquo;
                <img src="{{ asset('assets/img/layout/infinity-spaces.png') }}" alt="">
            </a>
        </div>
    </section>

    <section id="business-center">
        <div class="center">
            <h2>{{ trans('frontend.business-center.titulo') }}</h2>

            <div class="mapa mapa-1">
                <ul class="mapa-list">
                    <li class="mapa-handle escritorios" data-imagem="mapa23/escritorios">
                        <span>{{ trans('frontend.business-center.escritorios') }}</span>
                    </li>
                    <li class="mapa-handle recepcao" data-imagem="mapa23/recepcao">
                        <span>{{ trans('frontend.business-center.recepcao') }}</span>
                    </li>
                    <li class="mapa-handle copa" data-imagem="mapa23/copa">
                        <span>{{ trans('frontend.business-center.copa') }}</span>
                    </li>
                    <li class="mapa-handle banheiros" data-imagem="mapa23/banheiros">
                        <span>{{ trans('frontend.business-center.banheiros') }}</span>
                    </li>
                    <li class="mapa-handle area-de-networking" data-imagem="mapa23/area-de-networking">
                        <span>{{ trans('frontend.business-center.area-networking') }}</span>
                    </li>
                    <li class="mapa-handle shared-space" data-imagem="mapa23/shared-space">
                        <span>{{ trans('frontend.business-center.shared-space') }}</span>
                    </li>
                    <li class="mapa-handle coworking" data-imagem="mapa23/coworking">
                        <span>{{ trans('frontend.business-center.coworking') }}</span>
                    </li>
                    <li class="mapa-handle salas-de-reuniao" data-imagem="mapa23/salas-de-reuniao">
                        <span>{{ trans('frontend.business-center.salas-reuniao') }}</span>
                    </li>
                    <li class="mapa-handle elevadores" data-imagem="mapa23/elevadores">
                        <span>{{ trans('frontend.business-center.elevadores') }}</span>
                    </li>
                    <li class="mapa-handle copiadora" data-imagem="mapa23/copiadora">
                        <span>{{ trans('frontend.business-center.copiadora') }}</span>
                    </li>
                    <li class="mapa-handle deposito" data-imagem="mapa23/deposito">
                        <span>{{ trans('frontend.business-center.deposito') }}</span>
                    </li>
                    <li class="mapa-handle mail-box" data-imagem="mapa23/mail-box">
                        <span>{{ trans('frontend.business-center.mail-box') }}</span>
                    </li>
                    <li class="mapa-handle sala-zen" data-imagem="mapa23/sala-zen">
                        <span>{{ trans('frontend.business-center.sala-zen') }}</span>
                    </li>
                </ul>

                <div class="mapa-imagem">
                    <img src="{{ asset('assets/img/layout/mapa-23andar.png') }}" alt="">
                    <div class="mapa-imagem-hover"></div>
                    <div class="mapa-aviso">
                        <div class="mapa-aviso-wrapper">
                            <p>{{ trans('frontend.business-center.aviso') }}</p>
                        </div>
                    </div>
                    <div class="mapa-loading"></div>
                </div>
            </div>

            <div class="mapa mapa-2">
                <ul class="mapa-list">
                    <li class="mapa-handle salas" data-imagem="mapa24/administrativo">
                        <span>{{ trans('frontend.business-center.salas') }}</span>
                    </li>
                    <li class="mapa-handle recepcao" data-imagem="mapa24/recepcao">
                        <span>{{ trans('frontend.business-center.recepcao') }}</span>
                    </li>
                    <li class="mapa-handle copa" data-imagem="mapa24/copa">
                        <span>{{ trans('frontend.business-center.copa') }}</span>
                    </li>
                    <li class="mapa-handle banheiros" data-imagem="mapa24/banheiros">
                        <span>{{ trans('frontend.business-center.banheiros') }}</span>
                    </li>
                    <li class="mapa-handle showroom" data-imagem="mapa24/escritorios">
                        <span>{{ trans('frontend.business-center.escritorios') }}</span>
                    </li>
                    <li class="mapa-handle auditorio" data-imagem="mapa24/auditorio">
                        <span>{{ trans('frontend.business-center.auditorio') }}</span>
                    </li>
                    <li class="mapa-handle salas-de-reuniao" data-imagem="mapa24/salas-de-reuniao">
                        <span>{{ trans('frontend.business-center.salas-reuniao') }}</span>
                    </li>
                    <li class="mapa-handle area-operacional" data-imagem="mapa24/area-operacional">
                        <span>{{ trans('frontend.business-center.area-operacional') }}</span>
                    </li>
                    <li class="mapa-handle elevadores" data-imagem="mapa24/elevadores">
                        <span>{{ trans('frontend.business-center.elevadores') }}</span>
                    </li>
                </ul>

                <div class="mapa-imagem">
                    <img src="{{ asset('assets/img/layout/mapa-24andar.png') }}" alt="">
                    <div class="mapa-imagem-hover"></div>
                    <div class="mapa-loading"></div>
                    <div class="mapa-aviso">
                        <div class="mapa-aviso-wrapper">
                            <p>{{ trans('frontend.business-center.aviso') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="servicos">
        <div class="center">
            <h2>{{ trans('frontend.servicos.titulo') }}</h2>

            <div class="servicos-lista">
                @foreach($servicos as $servico)
                <div class="servicos-item">{{ $servico->{trans('banco.titulo')} }}</div>
                @endforeach
            </div>
        </div>
    </section>

    <section id="experimente">
        <div class="center">
            <h2>{{ trans('frontend.experimente.titulo') }}</h2>
            <h3>{{ trans('frontend.experimente.chamada') }}</h3>

            <a href="{{ route('experimente') }}" class="test-drive-link lightbox-content">{{ trans('frontend.experimente.test-drive') }}</a>

            <div class="experimente-galeria-wrapper">
                <div class="ilustracao">
                    <img src="{{ asset('assets/img/layout/ilustracao-composicao-marca.png') }}" alt="">
                </div>

                <div class="experimente-galeria">
                    @foreach($fotos['experimente'] as $foto)
                    <a href="{{ asset('assets/img/fotos/'.$foto->imagem) }}" rel="galeria-experimente" title="{{ $foto->legenda }}" class="lightbox-image">
                        <img src="{{ asset('assets/img/fotos/thumbs/'.$foto->imagem) }}" alt="">
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    <section id="indique">
        <div class="center">
            <h2>{{ trans('frontend.indique.titulo') }}</h2>
            <h3>{{ trans('frontend.indique.chamada') }}</h3>

            <form action="" id="form-indique" method="POST">
                <input type="email" name="por" id="por" placeholder="{{ trans('frontend.indique.por') }}" required>
                <input type="email" name="indicado" id="indicado" placeholder="{{ trans('frontend.indique.indicado') }}" required>
                <input type="submit" value="{{ trans('frontend.indique.indicar') }}">
                <div id="form-indique-response"></div>
            </form>
        </div>
    </section>

    <section id="contato">
        <div class="center">
            <h2>{{ trans('frontend.contato.titulo') }}</h2>

            <div class="contato-info">
                <?php $telefones = explode(',', $contato->telefone); ?>
                <p class="telefone">
                    @foreach($telefones as $telefone)
                    <?php
                        $telefone = explode(' ', trim($telefone));
                        $prefixos = implode(' ', array_splice($telefone, 0, 2));
                        $numero   = implode(' ', $telefone);
                    ?>
                    <span>
                        {{ $prefixos }} <strong>{{ $numero }}</strong>
                    </span>
                    @endforeach
                </p>
                {!! $contato->{trans('banco.endereco')} !!}
                <div class="googlemaps">{!! $contato->codigo_googlemaps !!}</div>
            </div>

            <div class="contato-form">
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                    <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                    <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                    <div id="form-contato-response"></div>
                </form>
            </div>

            <div class="contato-galeria">
                @foreach($fotos['contato'] as $foto)
                <a href="{{ asset('assets/img/fotos/'.$foto->imagem) }}" rel="galeria-contato" title="{{ $foto->legenda }}" class="lightbox-image">
                    <img src="{{ asset('assets/img/fotos/thumbs/'.$foto->imagem) }}" alt="">
                </a>
                @endforeach
            </div>
        </div>
    </section>

    {{--
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/584ac94de2def07b70ab0537/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    --}}

@endsection
