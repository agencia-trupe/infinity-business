<div class="lightbox-wrapper clientes-content">
    <h1>{{ trans('frontend.clientes') }}</h1>

    <div class="clientes-imagens">
        @foreach($clientes as $cliente)
        <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
        @endforeach
    </div>
</div>
