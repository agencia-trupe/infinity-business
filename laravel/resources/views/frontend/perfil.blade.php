<div class="lightbox-wrapper perfil-content">
    <h1>{{ $perfil->{trans('banco.nome')} }}</h1>

    <div class="texto">
        {!! $perfil->{trans('banco.texto')} !!}
    </div>

    <h4>{{ trans('frontend.perfis.planos') }}</h4>
    <div class="perfil-content-planos">
        @foreach($perfil->planos as $plano)
        <div class="plano">
            <img src="{{ asset('assets/img/layout/icone-'.($plano->tipo == 1 ? $plano->sigla : $plano->slug.'-laranja').'.png') }}" alt="">
            <p>{{ $plano->{trans('banco.nome')} }}</p>
        </div>
        @endforeach
    </div>
</div>
