<div class="lightbox-wrapper plano-content">
    <div class="plano-content-titulo">
        <img src="{{ asset('assets/img/layout/icone-'.($plano->tipo == 1 ? $plano->sigla : $plano->slug.'-laranja').'.png') }}" alt="">
        @if($plano->tipo == 1)
        <h1><span>{{ $plano->sigla }}</span> {{ $plano->{trans('banco.nome')} }}</h1>
        @else
        <h1><span>{{ $plano->{trans('banco.nome')} }}</span></h1>
        @endif
    </div>

    <div class="plano-content-chamada">
        <p>{{ $plano->{trans('banco.chamada')} }}</p>
    </div>

    <div class="texto">
        {!! $plano->{trans('banco.texto')} !!}
    </div>

    <div class="plano-form">
        <h3>{{ trans('frontend.planos.quero-saber') }}</h3>
        <form action="" id="form-plano" method="POST">
            <input type="hidden" name="assunto" id="plano_assunto" value="{{ $plano->{trans('banco.nome')} }}" required>
            <input type="text" name="nome" id="plano_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
            <input type="email" name="email" id="plano_email" placeholder="e-mail" required>
            <input type="text" name="telefone" id="plano_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
            <textarea name="mensagem" id="plano_mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
            <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
            <div id="form-plano-response"></div>
        </form>
    </div>
</div>
