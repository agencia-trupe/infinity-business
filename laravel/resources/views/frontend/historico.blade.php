<div class="lightbox-wrapper historico-content">
    <div class="aside">
        <h1>{{ trans('frontend.header.historico') }}</h1>
        <img src="{{ asset('assets/img/layout/marca-historico.png') }}" alt="">
    </div>

    <div class="texto">
        {!! $historico->{trans('banco.texto')} !!}
    </div>
</div>
