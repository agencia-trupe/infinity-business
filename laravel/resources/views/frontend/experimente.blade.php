<div class="lightbox-wrapper experimente-content">
    <form action="" id="form-experimente" method="POST">
        <p>{{ trans('frontend.experimente.titulo') }}</p>
        <div class="row">
            <input type="text" name="data" id="experimente_data" placeholder="{{ trans('frontend.experimente.data') }}" required>
            <label>
                <input type="checkbox" name="estacao_de_trabalho" id="experimente_estacao_de_trabalho" value="1">
                {{ trans('frontend.experimente.estacao') }}
            </label>
            <label>
                <input type="checkbox" name="sala_de_reuniao" id="experimente_sala_de_reuniao" value="1">
                {{ trans('frontend.experimente.sala') }}
            </label>
        </div>
        <input type="text" name="nome" id="experimente_nome" placeholder="{{ trans('frontend.experimente.nome') }}" required>
        <input type="email" name="e_mail" id="experimente_e_mail" placeholder="{{ trans('frontend.experimente.email') }}" required>
        <input type="text" name="telefone" id="experimente_telefone" placeholder="{{ trans('frontend.experimente.telefone') }}">
        <div id="form-experimente-response"></div>
        <input type="submit" value="{{ trans('frontend.experimente.enviar') }}">
    </form>

    <p>{{ trans('frontend.experimente.explicacao') }}</p>
</div>
