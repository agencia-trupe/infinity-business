    <header>
        <div class="center">
            <a href="/" class="header-logo nav-scroll">{{ config('site.name') }}</a>
            <div class="nav-wrapper">
                <nav>
                    <a href="conceito" class="nav-scroll">{{ trans('frontend.header.conceito') }}</a>
                    <a href="espacos-servicos" class="nav-scroll">{{ trans('frontend.header.espacos-servicos') }}</a>
                    <a href="experimente" class="nav-scroll">{{ trans('frontend.header.experimente') }}</a>
                    <a href="{{ route('historico') }}" class="lightbox-content">{{ trans('frontend.header.historico') }}</a>
                    <a href="{{ route('clientes') }}" class="lightbox-content">{{ trans('frontend.header.clientes') }}</a>
                    <a href="{{ route('midia') }}" class="lightbox-content">{{ trans('frontend.header.midia') }}</a>
                    <a href="http://coworkingglobal.com.br/" target="_blank">BLOG</a>
                    <a href="contato" class="nav-scroll">{{ trans('frontend.header.contato') }}</a>
                </nav>

                <div class="links-social">
                    @foreach(['facebook', 'instagram'] as $s)
                        @if($contato->{$s})
                        <a href="{{ $contato->{$s} }}" class="social {{ $s }}">{{ $s }}</a>
                        @endif
                    @endforeach
                </div>

                <div class="telefones">
                    <?php $telefones = explode(',', $contato->telefone); ?>
                    @foreach(collect($telefones)->take(2) as $telefone)
                    <span>{{ trim($telefone) }}</span>
                    @endforeach
                </div>

                <div class="lang-wrapper">
                    @foreach([
                        'pt' => 'Versão em Português',
                        'en' => 'English Version',
                        'es' => 'Versión en Español'
                    ] as $key => $title)
                        @unless(app()->getLocale() == $key)
                        <a href="{{ route('lang', $key) }}" class="lang lang-{{ $key }}">
                            {{ $title }}
                        </a>
                        @endunless
                    @endforeach
                </div>

                <div class="extras">
                    <a href="https://infinity.opensev.com.br/index.php?r=site/login" target="_blank">
                        {{ trans('frontend.header.portal') }}
                    </a>
                    <a href="{{ route('imovel') }}" class="imovel lightbox-content">
                        {{ trans('frontend.header.imovel') }}
                    </a>
                </div>
            </div>
            <a href="#" id="mobile-handle">MENU</a>
        </div>

        <nav id="nav-mobile">
            <a href="conceito" class="nav-scroll">{{ trans('frontend.header.conceito') }}</a>
            <a href="espacos-servicos" class="nav-scroll">{{ trans('frontend.header.espacos-servicos') }}</a>
            <a href="experimente" class="nav-scroll">{{ trans('frontend.header.experimente') }}</a>
            <a href="{{ route('historico') }}" class="lightbox-content">{{ trans('frontend.header.historico') }}</a>
            <a href="{{ route('clientes') }}" class="lightbox-content">{{ trans('frontend.header.clientes') }}</a>
            <a href="{{ route('midia') }}" class="lightbox-content">{{ trans('frontend.header.midia') }}</a>
            <a href="http://coworkingglobal.com.br/" target="_blank">BLOG</a>
            <a href="contato" class="nav-scroll">{{ trans('frontend.header.contato') }}</a>
            <a href="https://infinity.opensev.com.br/index.php?r=site/login" target="_blank">
                {{ trans('frontend.header.portal') }}
            </a>
            <a href="{{ route('imovel') }}" class="imovel lightbox-content">
                {{ trans('frontend.header.imovel') }}
            </a>
        </nav>
    </header>
