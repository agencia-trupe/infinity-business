    <footer>
        <div class="footer-info">
            <div class="center">
                <div class="links-col">
                    <a href="conceito" class="nav-scroll">» {{ trans('frontend.header.conceito') }}</a>
                    <a href="espacos-servicos" class="nav-scroll">» {{ trans('frontend.header.espacos-servicos') }}</a>
                    <a href="experimente" class="nav-scroll">» {{ trans('frontend.header.experimente') }}</a>
                    <a href="{{ route('historico') }}" class="lightbox-content">» {{ trans('frontend.header.historico') }}</a>
                    <a href="{{ route('clientes') }}" class="lightbox-content">» {{ trans('frontend.header.clientes') }}</a>
                    <a href="{{ route('midia') }}" class="lightbox-content">» {{ trans('frontend.header.midia') }}</a>
                    <a href="http://coworkingglobal.com.br/" target="_blank">» BLOG</a>
                    <a href="contato" class="nav-scroll">» {{ trans('frontend.header.contato') }}</a>
                </div>

                <div class="links-col">
                    @foreach($planos->where('tipo', 1) as $plano)
                    <a href="{{ route('plano', $plano->slug) }}" class="lightbox-content">» {{ $plano->sigla }} &middot; {{ $plano->{trans('banco.nome')} }}</a>
                    @endforeach
                </div>

                <div class="links-col">
                    @foreach($planos->where('tipo', 2) as $plano)
                    <a href="{{ route('plano', $plano->slug) }}" class="lightbox-content">» {{ $plano->{trans('banco.nome')} }}</a>
                    @endforeach
                </div>

                <div class="footer-contato">
                    <img src="{{ asset('assets/img/layout/marca-rodape.png') }}" alt="">
                    <div class="footer-contato-texto">
                        <?php $telefones = explode(',', $contato->telefone); ?>
                        <p class="telefone">
                            @foreach($telefones as $telefone)
                            <?php
                                $telefone = explode(' ', trim($telefone));
                                $prefixos = implode(' ', array_splice($telefone, 0, 2));
                                $numero   = implode(' ', $telefone);
                            ?>
                            <span>
                                {{ $prefixos }} <strong>{{ $numero }}</strong>
                            </span>
                            @endforeach
                        </p>
                        {!! $contato->{trans('banco.endereco')} !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-copyright">
            <div class="center">
                <p>© {{ date('Y') }} {{ config('site.name') }} &middot; {{ trans('frontend.footer.direitos') }} <span class="div"></span> <a href="http://trupe.net" target="_blank">{{ trans('frontend.footer.criacao-sites') }}</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a></p>
            </div>
        </div>
    </footer>
