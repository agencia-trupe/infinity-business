<div class="lightbox-wrapper experimente-content">
    <form action="" id="form-imovel" method="POST">
        <p>
            {{ trans('frontend.header.imovel') }}<br>
            {{ trans('frontend.contato.por-aqui') }}
        </p>
        <input type="text" name="nome" id="imovel_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
        <input type="email" name="e_mail" id="imovel_e_mail" placeholder="e-mail" required>
        <input type="text" name="telefone" id="imovel_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
        <input type="text" name="localizacao" id="imovel_localizacao" placeholder="{{ trans('frontend.contato.localizacao-imovel') }}" required>
        <div id="form-imovel-response"></div>
        <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
    </form>
</div>
