(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.shrinkHeader = function() {
        $(window).on('scroll', function(event) {
            var distance  = $(document).scrollTop(),
                threshold = $('#conceito').offset().top - 120,
                header    = $('header');

            if (distance > threshold && $(window).width() > 1179) {
                header.addClass('shrink');
            } else {
                header.removeClass('shrink');
            }
        });
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-handle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.navScroll = function() {
        $('.nav-scroll').on('click touchstart', function(event) {
            event.preventDefault();

            var section = $(this).attr('href');
            if (section === 'espacos-servicos') section = 'planos';
            if (section === '/') section = 'top';

            $('html, body').animate({
                scrollTop: $('#' + section).offset().top - ($(window).width() > 1179 ? 99 : 0)
            });

            if ($(this).parent().is('#nav-mobile')) {
                $('#nav-mobile').slideToggle();
                $('#mobile-handle').toggleClass('close');
            }
        });
    };

    App.videoHeight = function() {
        $(window).resize(function() {
            if ($(window).width() > 1180) {
                $('#video').css('height', $(window).innerHeight() - 120);
            } else {
                $('#video').css('height', '');
            }
        });

        $(window).trigger('resize');
    };

    App.showVideo = function() {
        $('.video-handle').click(function(event) {
            event.preventDefault();

            $('.video-player').fadeIn();
        });
    };

    App.toggleFacilities = function() {
        $('.facilities-handle').click(function(event) {
            event.preventDefault();

            $(this).toggleClass('open');
            $('.facilities-texto').slideToggle();
        });
    };

    App.imageLightbox = function() {
        $('.lightbox-image').fancybox({
            helpers: {
                overlay: {
                    locked: false,
                    css: {'background-color': 'rgba(0,0,0,.5)'},
                },
                title: {
                    type: 'inside'
                }
            },
            maxHeight: '85%',
            padding: 10
        });
    };

    App.contentLightbox = function() {
        $('.lightbox-content').fancybox({
            helpers: {
                overlay: {
                    locked: false,
                    css: {'background-color': 'rgba(0,0,0,.5)'},
                }
            },
            type: 'ajax',
            maxWidth: 980,
            maxHeight: '95%',
            padding: 0
        });
    };

    App.carouselExperimente = function() {
        $('.experimente-galeria').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':'formulário-contato/enviar',
                    'virtualPageTitle' : 'contato enviado com sucesso'
                }); 

                window.google_trackConversion({
                    google_conversion_id: 875868630,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "bxVdCNrflWsQ1uPSoQM",
                    google_remarketing_only: false,
                });

                $response.fadeOut().text(data.message).fadeIn('slow');
                location.href = $('base').attr('href') + '/contato/obrigado';
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioContatoPlano = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-plano-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato-plano',
            data: {
                assunto: $('#plano_assunto').val(),
                nome: $('#plano_nome').val(),
                email: $('#plano_email').val(),
                telefone: $('#plano_telefone').val(),
                mensagem: $('#plano_mensagem').val(),
            },
            success: function(data) {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':'formulário-contato/enviar',
                    'virtualPageTitle' : 'contato enviado com sucesso'
                }); 

                window.google_trackConversion({
                    google_conversion_id: 875868630,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "bxVdCNrflWsQ1uPSoQM",
                    google_remarketing_only: false,
                });

                $response.fadeOut().text(data.message).fadeIn('slow');
                location.href = $('base').attr('href') + '/contato-plano/obrigado';
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioIndicacao = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-indique-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/indique',
            data: {
                por: $('#por').val(),
                indicado: $('#indicado').val(),
            },
            success: function(data) {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':'formulário-indicação/enviar',
                    'virtualPageTitle' : 'indicação enviada com sucesso'
                });

                window.google_trackConversion({
                    google_conversion_id: 875868630,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "bxVdCNrflWsQ1uPSoQM",
                    google_remarketing_only: false,
                });

                $response.fadeOut().text(data.message).fadeIn('slow');
                location.href = $('base').attr('href') + '/indique/obrigado';
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioSolicitacao = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-experimente-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/experimente',
            data: {
                data: $('#experimente_data').val(),
                estacao_de_trabalho: $('#experimente_estacao_de_trabalho').is(':checked') ? 1 : 0,
                sala_de_reuniao: $('#experimente_sala_de_reuniao').is(':checked') ? 1 : 0,
                nome: $('#experimente_nome').val(),
                e_mail: $('#experimente_e_mail').val(),
                telefone: $('#experimente_telefone').val(),
            },
            success: function(data) {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':'formulário-experimente/enviar',
                    'virtualPageTitle' : 'test drive enviado com sucesso'
                });

                window.google_trackConversion({
                    google_conversion_id: 875868630,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "bxVdCNrflWsQ1uPSoQM",
                    google_remarketing_only: false,
                });

                $response.fadeOut();
                location.href = $('base').attr('href') + '/experimente/obrigado';
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioImovel = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-imovel-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/imovel',
            data: {
                nome: $('#imovel_nome').val(),
                e_mail: $('#imovel_e_mail').val(),
                telefone: $('#imovel_telefone').val(),
                localizacao: $('#imovel_localizacao').val(),
            },
            success: function(data) {
                dataLayer.push({
                    'event':'VirtualPageview',
                    'virtualPageURL':'formulário-imovel/enviar',
                    'virtualPageTitle' : 'contato de imóvel enviado com sucesso'
                });

                window.google_trackConversion({
                    google_conversion_id: 875868630,
                    google_conversion_language: "en",
                    google_conversion_format: "3",
                    google_conversion_color: "ffffff",
                    google_conversion_label: "bxVdCNrflWsQ1uPSoQM",
                    google_remarketing_only: false,
                });

                $response.fadeOut();
                location.href = $('base').attr('href') + '/imovel/obrigado';
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.mapaHoverSections = function() {
        $('.mapa-handle a').on('click', function(event) {
            event.preventDefault();
        });
        $('.mapa-handle').on('click', function(event) {
            event.preventDefault();

            var $this = $(this);
            if ($this.hasClass('active')) return;

            var $loading  = $this.parent().next().find('.mapa-loading'),
                $hoverDiv = $this.parent().next().find('.mapa-imagem-hover'),
                $aviso    = $this.parent().next().find('.mapa-aviso');

            $this.siblings().removeClass('active');
            $this.addClass('active');
            $hoverDiv.fadeOut('fast');
            $aviso.fadeOut('fast');
            $loading.fadeIn();

            var newImg = new Image();
            newImg.onload = function() {
                $loading.fadeOut('fast', function() {
                    $hoverDiv.css('background-image', 'url(' + newImg.src + ')').fadeIn(350);
                });
            };
            newImg.onerror = function() {
                $loading.fadeOut('fast');
                $this.removeClass('active');
            };
            newImg.src = $('base').attr('href') + '/assets/img/layout/' + $this.data('imagem') + '.png';
        });
        $('.mapa-aviso').on('click', function(event) {
            $(this).fadeOut();
        });
    };

    App.init = function() {
        this.shrinkHeader();
        this.mobileToggle();
        this.navScroll();
        this.videoHeight();
        this.showVideo();
        this.toggleFacilities();
        this.imageLightbox();
        this.contentLightbox();
        this.carouselExperimente();
        $('#form-contato').on('submit', this.envioContato);
        $('#form-indique').on('submit', this.envioIndicacao);
        $(document).on('submit', '#form-plano', this.envioContatoPlano);
        $(document).on('submit', '#form-experimente', this.envioSolicitacao);
        $(document).on('submit', '#form-imovel', this.envioImovel);
        this.mapaHoverSections();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
