<?php

return [

    'endereco'    => 'endereco_en',
    'texto'       => 'texto_en',
    'texto_1'     => 'texto_1_en',
    'texto_2'     => 'texto_2_en',
    'titulo'      => 'titulo_en',
    'nome'        => 'nome_en',
    'chamada'     => 'chamada_en',
    'comparativo' => 'comparativo_en',
    'video_codigo' => 'video_codigo_en'

];
