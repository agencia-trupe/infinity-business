<?php

return [

    'header' => [
        'conceito'         => 'CONCEPT',
        'espacos-servicos' => 'SPACES + SERVICES',
        'experimente'      => 'TRY IT',
        'historico'        => 'HISTORY',
        'clientes'         => 'CLIENTS',
        'midia'            => 'MEDIA',
        'contato'          => 'CONTACT',
        'portal'           => 'CLIENT AREA',
        'imovel'           => 'DO YOU HAVE A PROPERTY AND WANT TO BUILD A COWORKING?',
    ],

    'clientes' => 'INFINITY CLIENTS',

    'video' => [
        'titulo'     => 'DISCOVER A NEW CONCEPT OF COWORKING IN BRAZIL',
        'subtitulo'  => 'in Alphaville &middot; SP',
        'clique'     => 'CLICK AND WATCH HOW THIS SOLUTION WORKS',
        'contrate'   => 'GET YOURS NOW',
        'saiba-mais' => 'more information',
    ],

    'conceito' => [
        'titulo'          => '&middot; SPACE &middot; SERVICES &middot; TECHNOLOGY &middot; NETWORK &middot;',
        'escritorios'     => 'fully equipped offices',
        'escritorios-sub' => 'high standard',
        'identidade'      => 'business identity',
        'identidade-sub'  => 'branding + marketing',
        'eventos'         => 'business events',
        'eventos-sub'     => 'networking',
        'facilidades'     => 'facilities and flexibility',
        'facilidades-sub' => 'technology + services',
    ],

    'facilities' => [
        'titulo'     => 'FACILITIES',
        'saiba-mais' => 'More information',
    ],

    'perfis' => [
        'titulo' => 'WHAT IS YOUR COMPANY PROFILE?',
        'planos' => 'Plans and services that might help you:',
    ],

    'planos' => [
        'titulo'       => 'PLANS',
        'a-partir'     => 'from',
        'por-estacao'  => 'for workstation',
        'mes'          => 'month',
        'saber-mais'   => 'LEARN MORE',
        'saber-mais-2' => 'LEARN<br> MORE',
        'quero-saber'  => 'I WANT TO LEARN MORE',
    ],

    'compare' => [
        'titulo'          => 'COMPARE',
        'tabela-titulo'   => 'Services | Plans',
        'tabela-legenda'  => '(*) According to availability and current price list.',
        'infinity-spaces' => 'See more architectural projects and interior design'
    ],

    'business-center' => [
        'titulo'          => 'THE BUSINESS CENTER',
        'aviso'           => 'Click on the legend beside to see the locations of the services on the map',
        'escritorios'     => 'Offices',
        'recepcao'        => 'Reception',
        'copa'            => 'Lounge/Catering Service',
        'banheiros'       => 'Bathrooms',
        'area-networking' => 'Networking Area',
        'shared-space'    => 'Shared Space',
        'coworking'       => 'Coworking',
        'salas-reuniao'   => 'Meeting Rooms',
        'elevadores'      => 'Lifts',
        'copiadora'       => 'Printing Area',
        'deposito'        => 'Deposit',
        'mail-box'        => 'Mail Box',
        'sala-zen'        => 'Zen Room',
        'salas'           => 'Administration',
        'espaco-social'   => 'Social Area',
        'showroom'        => 'Showroom',
        'auditorio'       => 'Auditorium',
        'area-operacional' => 'Operational Area',
    ],

    'servicos' => [
        'titulo' => 'INFINITY ADVANTAGES',
    ],

    'experimente' => [
        'titulo'      => 'TRY IT',
        'chamada'     => 'Schedule your meeting room or workstation here!',
        'test-drive'  => 'FREE TEST-DRIVE',
        'form-titulo' => 'ENTER THE DATE OF THE DESIRED SERVICES:',
        'data'        => 'DATE',
        'estacao'     => 'WORKSTATION',
        'sala'        => 'MEETING ROOM',
        'nome'        => 'your name',
        'email'       => 'your e-mail',
        'telefone'    => 'your contact phone number',
        'enviar'      => 'SEND REQUEST',
        'explicacao'  => 'We´ll contact you with the confirmation after request analysis.',
        'sucesso'     => 'Request sent successfully!',
    ],

    'indique' => [
        'titulo'   => 'INDICATE A CLIENT',
        'chamada'  => 'Meet our indication policy.',
        'por'      => 'your e-mail',
        'indicado' => 'e-mail of the indicated person or company',
        'indicar'  => 'SEND',
        'sucesso'  => 'Indication sent successfully!',
    ],

    'contato' => [
        'titulo'   => 'ADDRESS &middot; CONTACT',
        'nome'     => 'name',
        'telefone' => 'phone number',
        'mensagem' => 'message',
        'enviar'   => 'send',
        'sucesso'  => 'Message sent successfully!',
        'por-aqui' => 'CONTACT US HERE',
        'localizacao-imovel' => 'property location',
    ],

    'footer' => [
        'direitos'      => 'All rights reserved',
        'criacao-sites' => 'Website creation',
    ],

    'form-erro' => 'Fill in all required fields',
    'voltar'    => 'Return',

    'voltar-home' => 'GO BACK',

];
