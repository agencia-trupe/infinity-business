<?php

return [

    'header' => [
        'conceito'         => 'CONCEITO',
        'espacos-servicos' => 'ESPAÇOS + SERVIÇOS',
        'experimente'      => 'EXPERIMENTE',
        'historico'        => 'HISTÓRICO',
        'clientes'         => 'CLIENTES',
        'midia'            => 'MÍDIA',
        'contato'          => 'CONTATO',
        'portal'           => 'PORTAL DO CLIENTE',
        'imovel'           => 'TEM UM IMÓVEL E QUER MONTAR UM COWORKING?',
    ],

    'clientes' => 'CLIENTES INFINITY',

    'video' => [
        'titulo'     => 'CONHEÇA UM NOVO CONCEITO DE COWORKING NO BRASIL',
        'subtitulo'  => 'em Alphaville &middot; SP',
        'clique'     => 'CLIQUE E ASSISTA COMO FUNCIONA ESSA SOLUÇÃO',
        'contrate'   => 'CONTRATE AGORA O SEU',
        'saiba-mais' => 'saiba mais',
    ],

    'conceito' => [
        'titulo'          => '&middot; ESPAÇO &middot; SERVIÇOS &middot; TECNOLOGIA &middot; NETWORK &middot;',
        'escritorios'     => 'escritórios prontos',
        'escritorios-sub' => 'alto padrão',
        'identidade'      => 'identidade empresarial',
        'identidade-sub'  => 'branding + marketing',
        'eventos'         => 'eventos de negócios',
        'eventos-sub'     => 'networking',
        'facilidades'     => 'facilidades e flexibilidade',
        'facilidades-sub' => 'tecnologia + serviços',
    ],

    'facilities' => [
        'titulo'     => 'FACILITIES',
        'saiba-mais' => 'Saiba mais',
    ],

    'perfis' => [
        'titulo' => 'QUAL O PERFIL DA SUA EMPRESA?',
        'planos' => 'Planos e serviços que podem ajudá-lo(a):',
    ],

    'planos' => [
        'titulo'       => 'PLANOS',
        'a-partir'     => 'a partir de',
        'por-estacao'  => 'por estação de trabalho',
        'mes'          => 'mês',
        'saber-mais'   => 'SABER MAIS',
        'saber-mais-2' => 'SABER<br> MAIS',
        'quero-saber'  => 'QUERO SABER MAIS',
    ],

    'compare' => [
        'titulo'          => 'COMPARE',
        'tabela-titulo'   => 'Serviços | Planos',
        'tabela-legenda'  => '(*) De acordo com tabela de preços vigente e disponibilidade.',
        'infinity-spaces' => 'Saiba mais sobre nossos serviços de Projeto e Design de Interiores'
    ],

    'business-center' => [
        'titulo'          => 'O BUSINESS CENTER',
        'aviso'           => 'Clique na legenda ao lado para ver as localizações dos serviços no mapa',
        'escritorios'     => 'Escritórios',
        'recepcao'        => 'Recepção',
        'copa'            => 'Lounge/Copa',
        'banheiros'       => 'Banheiros',
        'area-networking' => 'Área de Networking',
        'shared-space'    => 'Shared Space',
        'coworking'       => 'Coworking',
        'salas-reuniao'   => 'Salas de Reunião',
        'elevadores'      => 'Elevadores',
        'copiadora'       => 'Copiadora',
        'deposito'        => 'Depósito',
        'mail-box'        => 'Mail Box',
        'sala-zen'        => 'Sala Zen',
        'salas'           => 'Área Administrativa',
        'espaco-social'   => 'Espaço Social',
        'showroom'        => 'Showroom',
        'auditorio'       => 'Auditório',
        'area-operacional' => 'Área Operacional',
    ],

    'servicos' => [
        'titulo' => 'VANTAGENS INFINITY',
    ],

    'experimente' => [
        'titulo'      => 'EXPERIMENTE',
        'chamada'     => 'Agende aqui a sua hora de reunião ou de estação de trabalho por nossa conta!',
        'test-drive'  => 'TEST-DRIVE GRATUITO',
        'form-titulo' => 'INFORME A DATA E SERVIÇO DESEJADOS:',
        'data'        => 'DATA',
        'estacao'     => 'ESTAÇÃO DE TRABALHO',
        'sala'        => 'SALA DE REUNIÃO',
        'nome'        => 'seu nome',
        'email'       => 'seu e-mail',
        'telefone'    => 'seu telefone para contato',
        'enviar'      => 'ENVIAR SOLICITAÇÃO',
        'explicacao'  => 'Você receberá contato com a confirmação após análise da solicitação.',
        'sucesso'     => 'Solicitação enviada com sucesso!',
    ],

    'indique' => [
        'titulo'   => 'INDIQUE UM CLIENTE',
        'chamada'  => 'Conheça nossa política de indicações.',
        'por'      => 'seu e-mail',
        'indicado' => 'e-mail do cliente indicado',
        'indicar'  => 'INDICAR',
        'sucesso'  => 'Indicação enviada com sucesso!',
    ],

    'contato' => [
        'titulo'   => 'LOCALIZAÇÃO &middot; CONTATO',
        'nome'     => 'nome',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
        'enviar'   => 'enviar',
        'sucesso'  => 'Contato enviado com sucesso!',
        'por-aqui' => 'ENTRE EM CONTATO POR AQUI',
        'localizacao-imovel' => 'localização do imóvel',
    ],

    'footer' => [
        'direitos'      => 'Todos os direitos reservados',
        'criacao-sites' => 'Criação de sites',
    ],

    'form-erro' => 'Preencha todos os campos corretamente',
    'voltar'    => 'Voltar',

    'voltar-home' => 'VOLTAR PARA A HOME',

];
