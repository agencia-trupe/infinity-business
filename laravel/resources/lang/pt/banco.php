<?php

return [

    'endereco'     => 'endereco',
    'texto'        => 'texto',
    'texto_1'      => 'texto_1',
    'texto_2'      => 'texto_2',
    'titulo'       => 'titulo',
    'nome'         => 'nome',
    'chamada'      => 'chamada',
    'comparativo'  => 'comparativo',
    'video_codigo' => 'video_codigo'

];
