<?php

return [

    'header' => [
        'conceito'         => 'CONCEPTO',
        'espacos-servicos' => 'ESPACIOS + SERVICIOS',
        'experimente'      => 'PRUEBE',
        'historico'        => 'HISTORIAL',
        'clientes'         => 'CLIENTES',
        'midia'            => 'PRENSA',
        'contato'          => 'CONTACTO',
        'portal'           => 'PORTAL DEL CLIENTE',
        'imovel'           => '¿TIENE UN INMUEBLE Y QUÉ MONTAR UN COWORKING?',
    ],

    'clientes' => 'CLIENTES INFINITY',

    'video' => [
        'titulo'     => 'CONOZCA UN NUEVO CONCEPTO DE COWORKING EN BRASIL',
        'subtitulo'  => 'en Alphaville &middot; SP',
        'clique'     => 'CLIC Y ASISTA COMO FUNCIONA ESTA SOLUCIÓN',
        'contrate'   => 'CONTRATE SU PLAN',
        'saiba-mais' => 'sepa más',
    ],

    'conceito' => [
        'titulo'          => '&middot; ESPACIO &middot; SERVICIOS &middot; TECNOLOGÍA &middot; NETWORK &middot;',
        'escritorios'     => 'oficinas listas para usar',
        'escritorios-sub' => 'alto nivel',
        'identidade'      => 'identidad empresarial',
        'identidade-sub'  => 'branding + marketing',
        'eventos'         => 'eventos de negócios',
        'eventos-sub'     => 'networking',
        'facilidades'     => 'facilidades y flexibilidade',
        'facilidades-sub' => 'tecnología + servicios',
    ],

    'facilities' => [
        'titulo'     => 'INSTALACIONES',
        'saiba-mais' => 'Sepa más',
    ],

    'perfis' => [
        'titulo' => '¿CUÁL ES EL PERFIL DE SU EMPRESA?',
        'planos' => 'Planes y servicios que pueden ayudarle:',
    ],

    'planos' => [
        'titulo'       => 'Planes',
        'a-partir'     => 'desde',
        'por-estacao'  => 'por estación de trabajo',
        'mes'          => 'mes',
        'saber-mais'   => 'SEPA MÁS',
        'saber-mais-2' => 'SEPA<br> MÁS',
        'quero-saber'  => 'QUIERO SEPA MÁS',
    ],

    'compare' => [
        'titulo'          => 'COMPARE',
        'tabela-titulo'   => 'Servicios | Planes',
        'tabela-legenda'  => '(*) Según tabla de precios vigente y disponibilidad.',
        'infinity-spaces' => 'Más información sobre nuestros servicios de diseño y diseño de interiores'
    ],

    'business-center' => [
        'titulo'          => 'BUSINESS CENTER',
        'aviso'           => 'Clic en la leyenda al lado para ver las ubicaciones de los servicios en el mapa',
        'escritorios'     => 'Oficinas',
        'recepcao'        => 'Recepción',
        'copa'            => 'Café',
        'banheiros'       => 'Baños',
        'area-networking' => 'Networking',
        'shared-space'    => 'Shared spaces',
        'coworking'       => 'Coworking',
        'salas-reuniao'   => 'Salas de reunión',
        'elevadores'      => 'Ascensores',
        'copiadora'       => 'Copiador',
        'deposito'        => 'Depósitos',
        'mail-box'        => 'Courier',
        'sala-zen'        => 'Espacio Zen',
        'salas'           => 'Área administrativa',
        'espaco-social'   => 'Área común',
        'showroom'        => 'Showroom',
        'auditorio'       => 'Auditorio',
        'area-operacional' => 'Área operativa',
    ],

    'servicos' => [
        'titulo' => 'VENTAJAS INFINITY',
    ],

    'experimente' => [
        'titulo'      => 'PRUEBE',
        'chamada'     => '¡Solicite un horario de reunión o de estación de trabajo por nuestra cuenta!',
        'test-drive'  => 'PRUEBA GRATIS',
        'form-titulo' => 'INFORMACIÓN DE LA FECHA Y SERVICIO DESEADOS:',
        'data'        => 'FECHA',
        'estacao'     => 'ESTACIÓN DE TRABAJO',
        'sala'        => 'SALA DE REUNIÓN',
        'nome'        => 'su nombre',
        'email'       => 'su e-mail',
        'telefone'    => 'su teléfono de contacto',
        'enviar'      => 'ENVIAR SOLICITUD',
        'explicacao'  => 'Usted recibirá contacto con la confirmación después de analizar la solicitud.',
        'sucesso'     => 'Solicitud enviada con éxito!',
    ],

    'indique' => [
        'titulo'   => 'INDIQUE UN CLIENTE',
        'chamada'  => 'Conozca nuestra política de indicaciones.',
        'por'      => 'su e-mail',
        'indicado' => 'e-mail del cliente indicado',
        'indicar'  => 'INDICAR',
        'sucesso'  => '¡Indicación enviada con éxito!',
    ],

    'contato' => [
        'titulo'   => 'LOCALIZACIÓN &middot; CONTACTO',
        'nome'     => 'nombre',
        'telefone' => 'teléfono',
        'mensagem' => 'mensaje',
        'enviar'   => 'enviar',
        'sucesso'  => 'Contacto enviado con éxito!',
        'por-aqui' => 'ENTRE EN CONTACTO POR AQUÍ',
        'localizacao-imovel' => 'ubicación del inmueble',
    ],

    'footer' => [
        'direitos'      => 'Todos los derechos reservados',
        'criacao-sites' => 'Creación de sitios',
    ],

    'form-erro' => 'Rellene todos los campos correctamente',
    'voltar'    => 'Volver',

    'voltar-home' => 'VOLVER AL HOME',

];
