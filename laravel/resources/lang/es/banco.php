<?php

return [

    'endereco'    => 'endereco_es',
    'texto'       => 'texto_es',
    'texto_1'     => 'texto_1_es',
    'texto_2'     => 'texto_2_es',
    'titulo'      => 'titulo_es',
    'nome'        => 'nome_es',
    'chamada'     => 'chamada_es',
    'comparativo' => 'comparativo_es',
    'video_codigo' => 'video_codigo_es'

];
