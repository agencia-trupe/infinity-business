<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('historico', 'HistoricoController@index')->name('historico');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('midia', 'MidiaController@index')->name('midia');
    Route::get('midia/{midia_id}', 'MidiaController@show')->name('midia.show');
    Route::get('perfil/{perfil_slug}', 'PerfilController@index')->name('perfil');
    Route::get('plano/{plano_slug}', 'PlanoController@index')->name('plano');
    Route::get('experimente', 'ExperimenteController@index')->name('experimente');
    Route::post('experimente', 'ExperimenteController@post')->name('experimente.post');
    Route::get('imovel', 'ImovelController@index')->name('imovel');
    Route::post('imovel', 'ImovelController@post')->name('imovel.post');
    Route::post('indique', 'IndiqueController@post')->name('indique.post');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('contato-plano', 'ContatoController@postPlano')->name('contato.postPlano');
    Route::get('contato/obrigado', function() {
        return view('frontend.obrigado.contato');
    });
    Route::get('contato-plano/obrigado', function() {
        return view('frontend.obrigado.contato-plano');
    });
    Route::get('indique/obrigado', function() {
        return view('frontend.obrigado.indique');
    });
    Route::get('experimente/obrigado', function() {
        return view('frontend.obrigado.experimente');
    });
    Route::get('imovel/obrigado', function() {
        return view('frontend.obrigado.imovel');
    });

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en', 'es'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::get('midia/delete-image/{midia_id}', 'MidiaController@deleteImage')->name('painel.midia.deleteImage');
        Route::get('midia/delete-pdf/{midia_id}', 'MidiaController@deletePdf')->name('painel.midia.deletePdf');
		Route::resource('midia', 'MidiaController');
        Route::get('midia/{midia}/imagens/clear', [
            'as'   => 'painel.midia.imagens.clear',
            'uses' => 'MidiaImagensController@clear'
        ]);
        Route::resource('midia.imagens', 'MidiaImagensController');
		Route::resource('video', 'VideoController', ['only' => ['index', 'update']]);
		Route::resource('historico', 'HistoricoController', ['only' => ['index', 'update']]);
		Route::resource('facilities', 'FacilitiesController', ['only' => ['index', 'update']]);
		Route::resource('comparativo', 'ComparativoController');
        Route::resource('clientes', 'ClientesController');
		Route::resource('perfis', 'PerfisController');
		Route::resource('planos', 'PlanosController');
		Route::resource('solicitacoes-de-test-drive', 'SolicitacoesDeTestDriveController');
		Route::resource('indicacoes', 'IndicacoesController');
		Route::resource('servicos', 'ServicosController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('imoveis', 'ImoveisRecebidosController');
        Route::resource('usuarios', 'UsuariosController');

        /* FOTOS */
        Route::get('fotos/galeria/{galeria}', 'FotosController@index')->name('painel.fotos.index');
        Route::get('fotos/{fotos}', 'FotosController@show')->name('painel.fotos.show');
        Route::post('fotos/{galeria}', 'FotosController@store')->name('painel.fotos.store');
        Route::patch('fotos/{fotos}', 'FotosController@update')->name('painel.fotos.update');
        Route::delete('fotos/{fotos}', 'FotosController@destroy')->name('painel.fotos.destroy');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
