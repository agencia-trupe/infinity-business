<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\SolicitacoesDeTestDriveRequest;
use App\Models\Solicitacao;
use App\Models\Contato;

class ExperimenteController extends Controller
{
    public function index()
    {
        return view('frontend.experimente');
    }

    public function post(SolicitacoesDeTestDriveRequest $request)
    {
        Solicitacao::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.solicitacao', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[SOLICITAÇÃO DE TEST-DRIVE] '.config('site.name'))
                        ->replyTo($request->get('e_mail'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.experimente.sucesso')
        ];

        return response()->json($response);
    }
}
