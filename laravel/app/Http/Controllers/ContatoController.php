<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    public function post(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function postPlano(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name').' - '.$request->get('assunto'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
