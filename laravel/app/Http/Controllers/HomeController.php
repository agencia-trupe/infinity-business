<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\Facilities;
use App\Models\Perfil;
use App\Models\Plano;
use App\Models\Comparativo;
use App\Models\Servico;
use App\Models\Contato;
use App\Models\Foto;

class HomeController extends Controller
{
    public function index()
    {
        $video       = Video::first();
        $facilities  = Facilities::first();
        $perfis      = Perfil::ordenados()->get();
        $planos      = Plano::orderBy('id', 'ASC')->get();
        $comparativo = Comparativo::with('planos')->ordenados()->get();
        $servicos    = Servico::ordenados()->get();
        $contato     = Contato::first();

        $fotos['conceito']    = Foto::where('galeria', 'conceito')->ordenados()->get();
        $fotos['experimente'] = Foto::where('galeria', 'experimente')->ordenados()->get();
        $fotos['contato']     = Foto::where('galeria', 'contato')->ordenados()->get();

        return view('frontend.home', compact('video', 'facilities', 'perfis', 'planos', 'comparativo', 'servicos', 'contato', 'fotos'));
    }
}
