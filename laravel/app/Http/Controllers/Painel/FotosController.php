<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FotosRequest;
use App\Http\Controllers\Controller;

use App\Models\Foto;

use App\Helpers\CropImage;

class FotosController extends Controller
{
    private $image_config = [
        [
            'width'  => 268,
            'height' => 178,
            'path'   => 'assets/img/fotos/thumbs/'
        ],
        [
            'width'  => 980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/fotos/'
        ]
    ];

    private $galerias = [
        'conceito'    => 8,
        'experimente' => 9,
        'contato'     => 5
    ];

    public function index($galeria = null)
    {
        if ( ! array_key_exists($galeria, $this->galerias)) return abort('404');

        $limite = $this->galerias[$galeria];
        $hideCreateBtn = Foto::where('galeria', $galeria)->count() >= $limite;

        $imagens = Foto::where('galeria', $galeria)->ordenados()->get();

        return view('painel.fotos.index', compact('imagens', 'galeria', 'hideCreateBtn', 'limite'));
    }

    public function show(Foto $imagem)
    {
        return $imagem;
    }

    public function store($galeria = null, FotosRequest $request)
    {
        if ( ! array_key_exists($galeria, $this->galerias)) return abort('404');

        $limite = $this->galerias[$galeria];

        if ($limite && Foto::where('galeria', $galeria)->count() >= $limite) {
            return 'Essa galeria já atingiu o limite máximo de fotos.';
        }

        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Foto::create($input);

            $imagem->galeria = $galeria;
            $imagem->save();

            $view = view('painel.fotos.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar foto: '.$e->getMessage();

        }
    }

    public function update(FotosRequest $request, Foto $imagem)
    {
        try {

            $imagem->update(['legenda' => $request->get('legenda')]);

            return response()->json(['success' => 'Legenda alterada com sucesso']);

        } catch (\Exception $e) {

            return 'Erro ao alterar legenda: '.$e->getMessage();

        }
    }

    public function destroy(Foto $imagem)
    {
        try {

            $imagem->delete();
            return back()->with('success', 'Foto excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir foto: '.$e->getMessage()]);

        }
    }
}
