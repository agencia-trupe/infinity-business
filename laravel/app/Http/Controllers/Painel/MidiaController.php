<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MidiaRequest;
use App\Http\Controllers\Controller;

use App\Models\Midia;
use App\Helpers\CropImage;

class MidiaController extends Controller
{
    private $image_config = [
        'width'  => 200,
        'height' => 200,
        'path'   => 'assets/img/midia/thumbs/'
    ];

    public function index()
    {
        $registros = Midia::orderBy('id', 'DESC')->get();

        return view('painel.midia.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.midia.create');
    }

    public function store(MidiaRequest $request)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);
            if ($request->hasFile('pdf')) $input['pdf'] = Midia::uploadPdf();

            Midia::create($input);
            return redirect()->route('painel.midia.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Midia $registro)
    {
        return view('painel.midia.edit', compact('registro'));
    }

    public function update(MidiaRequest $request, Midia $registro)
    {
        try {

            $input = $request->all();
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config);
            if ($request->hasFile('pdf')) $input['pdf'] = Midia::uploadPdf();

            $registro->update($input);
            return redirect()->route('painel.midia.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Midia $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.midia.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function deleteImage($id)
    {
        try {

            $midia = Midia::find($id);
            $midia->imagem = null;
            $midia->save();

            return back()->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }

    public function deletePdf($id)
    {
        try {

            $midia = Midia::find($id);
            $midia->pdf = null;
            $midia->save();

            return back()->with('success', 'Arquivo PDF excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir PDF: '.$e->getMessage()]);

        }
    }
}
