<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfisRequest;
use App\Http\Controllers\Controller;

use App\Models\Perfil;
use App\Models\Plano;

class PerfisController extends Controller
{
    public function index()
    {
        $registros = Perfil::ordenados()->get();

        return view('painel.perfis.index', compact('registros'));
    }

    public function create()
    {
        $planos = Plano::orderBy('id', 'ASC')->get();

        return view('painel.perfis.create', compact('planos'));
    }

    public function store(PerfisRequest $request)
    {
        try {

            $input  = $request->except('planos');
            $planos = ($request->get('planos') ?: []);

            $registro = Perfil::create($input);
            $registro->planos()->sync($planos);

            return redirect()->route('painel.perfis.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Perfil $registro)
    {
        $planos = Plano::orderBy('id', 'ASC')->get();

        return view('painel.perfis.edit', compact('registro', 'planos'));
    }

    public function update(PerfisRequest $request, Perfil $registro)
    {
        try {

            $input = $request->except('planos');
            $planos = ($request->get('planos') ?: []);

            $registro->update($input);
            $registro->planos()->sync($planos);

            return redirect()->route('painel.perfis.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Perfil $registro)
    {
        try {

            $registro->planos()->detach();
            $registro->delete();
            return redirect()->route('painel.perfis.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
