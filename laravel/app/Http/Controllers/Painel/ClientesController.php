<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;
use App\Helpers\CropImage;

class ClientesController extends Controller
{
    private $image_config = [
        'width'   => 185,
        'height'  => 120,
        'cliente' => true,
        'path'    => 'assets/img/clientes/'
    ];

    public function index()
    {
        $imagens = Cliente::ordenados()->get();

        return view('painel.clientes.index', compact('imagens'));
    }

    public function show(Cliente $imagem)
    {
        return $imagem;
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $imagem = Cliente::create($input);

            $view = view('painel.clientes.imagem', compact('imagem'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Cliente $imagem)
    {
        try {

            $imagem->delete();
            return redirect()->route('painel.clientes.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
