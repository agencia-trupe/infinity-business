<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\VideoRequest;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Helpers\CropImage;

class VideoController extends Controller
{
    private $image_config = [
        'width'  => 1280,
        'height' => null,
        'path'   => 'assets/img/video/'
    ];

    public function index()
    {
        $registro = Video::first();

        return view('painel.video.edit', compact('registro'));
    }

    public function update(VideoRequest $request, Video $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.video.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
