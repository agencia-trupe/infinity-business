<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\IndicacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Indicacao;

class IndicacoesController extends Controller
{
    public function index()
    {
        $registros = Indicacao::orderBy('id', 'DESC')->paginate(20);

        return view('painel.indicacoes.index', compact('registros'));
    }
}
