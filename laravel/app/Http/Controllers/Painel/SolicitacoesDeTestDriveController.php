<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SolicitacoesDeTestDriveRequest;
use App\Http\Controllers\Controller;

use App\Models\Solicitacao;

class SolicitacoesDeTestDriveController extends Controller
{
    public function index()
    {
        $registros = Solicitacao::orderBy('id', 'DESC')->paginate(10);

        return view('painel.solicitacoes-de-test-drive.index', compact('registros'));
    }

    public function show(Solicitacao $registro)
    {
        try {

            $registro->update([
                'lido' => $registro->lido ? 0 : 1
            ]);
            return back()->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Solicitacao $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.solicitacoes-de-test-drive.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
