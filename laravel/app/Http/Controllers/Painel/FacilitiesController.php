<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FacilitiesRequest;
use App\Http\Controllers\Controller;

use App\Models\Facilities;

class FacilitiesController extends Controller
{
    public function index()
    {
        $registro = Facilities::first();

        return view('painel.facilities.edit', compact('registro'));
    }

    public function update(FacilitiesRequest $request, Facilities $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.facilities.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
