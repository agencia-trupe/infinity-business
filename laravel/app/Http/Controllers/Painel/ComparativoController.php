<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ComparativoRequest;
use App\Http\Controllers\Controller;

use App\Models\Comparativo;
use App\Models\Plano;

class ComparativoController extends Controller
{
    public function index()
    {
        $registros = Comparativo::ordenados()->get();

        return view('painel.comparativo.index', compact('registros'));
    }

    public function create()
    {
        $planos = Plano::where('tipo', 1)->orderBy('id', 'ASC')->get();

        return view('painel.comparativo.create', compact('planos'));
    }

    public function store(ComparativoRequest $request)
    {
        try {

            $input  = $request->except('planos');
            $planos = ($request->get('planos') ?: []);

            $registro = Comparativo::create($input);
            $registro->planos()->sync($planos);

            return redirect()->route('painel.comparativo.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Comparativo $registro)
    {
        $planos = Plano::where('tipo', 1)->orderBy('id', 'ASC')->get();

        return view('painel.comparativo.edit', compact('registro', 'planos'));
    }

    public function update(ComparativoRequest $request, Comparativo $registro)
    {
        try {

            $input = $request->except('planos');
            $planos = ($request->get('planos') ?: []);

            $registro->update($input);
            $registro->planos()->sync($planos);

            return redirect()->route('painel.comparativo.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Comparativo $registro)
    {
        try {

            $registro->planos()->detach();
            $registro->delete();
            return redirect()->route('painel.comparativo.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
