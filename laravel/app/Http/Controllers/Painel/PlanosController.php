<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PlanosRequest;
use App\Http\Controllers\Controller;

use App\Models\Plano;

class PlanosController extends Controller
{
    public function index()
    {
        $registros = Plano::orderBy('id', 'ASC')->get();

        return view('painel.planos.index', compact('registros'));
    }

    public function edit(Plano $registro)
    {
        return view('painel.planos.edit', compact('registro'));
    }

    public function update(PlanosRequest $request, Plano $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.planos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }
}
