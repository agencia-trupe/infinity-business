<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosImovelRequest;
use App\Models\ImovelRecebido;
use App\Models\Contato;

class ImovelController extends Controller
{
    public function index()
    {
        return view('frontend.imovel');
    }

    public function post(ContatosRecebidosImovelRequest $request)
    {
        ImovelRecebido::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.imovel', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[IMÓVEL] '.config('site.name'))
                        ->replyTo($request->get('e_mail'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
