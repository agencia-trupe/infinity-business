<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index($slug = null)
    {
        $perfil = Perfil::with('planos')->where('slug', $slug)->first();
        if (! $perfil) abort('404');

        return view('frontend.perfil', compact('perfil'));
    }
}
