<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Midia;

class MidiaController extends Controller
{
    public function index()
    {
        $midia = Midia::ordenados()->get();

        return view('frontend.midia.index', compact('midia'));
    }

    public function show($id)
    {
        $midia = Midia::find($id);

        if (! $midia) abort('404');

        return view('frontend.midia.show', compact('midia'));
    }
}
