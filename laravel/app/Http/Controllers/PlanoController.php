<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Plano;

class PlanoController extends Controller
{
    public function index($slug = null)
    {
        $plano = Plano::where('slug', $slug)->first();
        if (! $plano) abort('404');

        return view('frontend.plano', compact('plano'));
    }
}
