<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\IndicacoesRequest;
use App\Models\Indicacao;

class IndiqueController extends Controller
{
    public function post(IndicacoesRequest $request)
    {
        Indicacao::create($request->all());

        \Mail::send('emails.indicacao', $request->all(), function($message) use ($request)
        {
            $message->to($request->get('indicado'), config('site.name'))
                    ->subject('[INDICAÇÃO] '.config('site.name'));
        });
        \Mail::send('emails.indicacao-aviso', $request->all(), function($message) use ($request)
        {
            $message->to('contato@infinitybusiness.com', config('site.name'))
                    ->subject('[INDICAÇÃO] '.config('site.name'));
        });

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.indique.sucesso')
        ];

        return response()->json($response);
    }
}
