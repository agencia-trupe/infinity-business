<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Historico;

class HistoricoController extends Controller
{
    public function index()
    {
        $historico = Historico::first();

        return view('frontend.historico', compact('historico'));
    }
}
