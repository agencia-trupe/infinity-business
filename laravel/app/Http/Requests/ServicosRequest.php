<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
