<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MidiaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'veiculo' => 'required',
            'titulo' => 'required',
            'imagem' => 'image',
            'pdf' => 'mimes:pdf',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
