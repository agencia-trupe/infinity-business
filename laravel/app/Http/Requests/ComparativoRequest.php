<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ComparativoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'comparativo' => 'required',
            'comparativo_en' => 'required',
            'comparativo_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
