<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'nome_en' => 'required',
            'nome_es' => 'required',
            'slug' => '',
            'texto' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
