<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class IndicacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'indicado' => 'email|required',
            'por' => 'email|required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function messages() {
        return [
            'required' => trans('frontend.form-erro'),
            'email'    => trans('frontend.form-erro'),
        ];
    }
}
