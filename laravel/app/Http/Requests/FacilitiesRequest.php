<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FacilitiesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1' => 'required',
            'texto_1_en' => 'required',
            'texto_1_es' => 'required',
            'texto_2' => 'required',
            'texto_2_en' => 'required',
            'texto_2_es' => 'required',
        ];
    }
}
