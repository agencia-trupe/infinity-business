<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosImovelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'        => 'required',
            'e_mail'       => 'required|email',
            'localizacao' => 'required'
        ];
    }


    public function messages() {
        return [
            'required' => trans('frontend.form-erro'),
            'email'    => trans('frontend.form-erro'),
        ];
    }
}
