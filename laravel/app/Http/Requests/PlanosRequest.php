<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => '',
            'slug' => '',
            'sigla' => '',
            'tipo' => '',
            'valor' => 'sometimes|required',
            'chamada_en' => 'required',
            'chamada_es' => 'required',
            'chamada' => 'required',
            'texto' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
