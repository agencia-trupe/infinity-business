<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SolicitacoesDeTestDriveRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'data' => 'required',
            'estacao_de_trabalho' => '',
            'sala_de_reuniao' => '',
            'nome' => 'required',
            'e_mail' => 'required|email',
            'telefone' => '',
            'lido' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function messages() {
        return [
            'required' => trans('frontend.form-erro'),
            'email'    => trans('frontend.form-erro'),
        ];
    }
}
