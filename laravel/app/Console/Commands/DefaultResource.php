<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DefaultResource extends Command
{
    use ResourceTrait;

    protected $signature = 'resource:default
                            {resourceName : Resource name}
                            {unitName : Unit name}
                            {fields : Table fields (name:type:validation)}
                            {--p|paginate= : Paginate}
                            {--s|sortable : Sortable}';

    protected $description = 'Generate default resource';

    public $resourceName;
    public $unitName;
    public $table;
    public $model;
    public $fields;
    public $paginate;
    public $sortable;

    public $namespace = 'Painel';
    public $templatesDir = 'resource.default';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->fetchArguments();
        $this->handleFields($this->fields);

        $this->displayFields();

        if ($this->confirm('Confirm?')) {
            $this->generate();
        }
    }

    protected function fetchArguments()
    {
        $this->resourceName = $this->argument('resourceName');
        $this->unitName     = $this->argument('unitName');
        $this->fields       = $this->argument('fields');

        $this->table = strtolower(str_slug($this->resourceName, '_'));
        $this->model = str_replace(' ', '', mb_convert_case(str_slug($this->unitName, ' '), MB_CASE_TITLE, 'UTF-8'));

        $this->paginate  = $this->option('paginate');
        $this->sortable  = $this->option('sortable');

        if ($this->paginate && $this->sortable) {
            throw new \Exception('A resource can\'t be paginated and sorted.');
        }
    }

    protected function handleFields($fields)
    {
        $fields = explode(',', $fields);

        foreach($fields as $key => $f) {
            $params = explode(':', $f);

            $fields[$key] = [
                'alias'      => (array_key_exists(0, $params) ? $params[0] : ''),
                'name'       => (array_key_exists(0, $params) ? str_slug($params[0], '_') : ''),
                'type'       => (array_key_exists(1, $params) ? $params[1] : ''),
                'validation' => (array_key_exists(2, $params) ? $params[2] : '')
            ];
        }

        $this->fields = $fields;
    }

    protected function displayFields()
    {
        $paginate = ($this->paginate ? ', paginated ('. $this->paginate .')' : '');
        $sortable = ($this->sortable ? ', sortable' : '');

        $this->output->writeln('Generate <comment>default'.$paginate.$sortable.'</comment> resource for model <info>'.$this->model.'</info> on table <info>'.$this->table.'</info>:'."\n");
        $this->table(['Alias', 'Name', 'Type', 'Validation'], $this->fields);
    }

    public function generate()
    {
        $this->generateMigration();
        $this->generateModel();
        $this->generateRoute();
        $this->generateRequest();
        $this->generateBinding();
        $this->generateController();
        $this->generateViews();
        $this->generateNav();
    }

    protected function generateMigration()
    {
        $migrationName  = date("Y_m_d_His") . '_create_' . $this->table . '_table';
        $migrationClass = 'Create' . $this->tableTitleCase() . 'Table';
        $migrationFile  = base_path('database/migrations')  . '/' . $migrationName . '.php';

        $content = view($this->templatesDir . '.migration', [
            'gen'            => $this,
            'migrationClass' => $migrationClass,
        ]);

        $this->createPhpFile($migrationFile, $content);
        $this->info("Migration file <comment>".$migrationName."</comment> generated successfully.");
    }

    protected function generateModel()
    {
        if ( ! file_exists(app_path('Models'))) mkdir(app_path('Models'), 0777, true);
        $modelFile = app_path('Models') . '/' . $this->model . '.php';

        if ($this->confirmOverwrite($modelFile)) {
            $content = view($this->templatesDir . '.model', [
                'gen' => $this
            ]);
            $this->createPhpFile($modelFile, $content);
            $this->info('Model class <comment>'.$this->model.'</comment> generated successfully.');
        }
    }

    protected function generateRoute()
    {
        $route = "Route::resource('{$this->routeName()}', '{$this->tableTitleCase()}Controller');";
        $routesFile = app_path('Http/routes.php');
        $routesFileContent = file_get_contents($routesFile);

        if (strpos($routesFileContent, $route)) {
            $this->info('Route already exists.');
            return false;
        }

        $commentHandle = $this->routesCommentHandle();
        if (strpos($routesFileContent, $commentHandle)) {
            $data = str_replace($commentHandle, "{$commentHandle}\n\t\t{$route}", $routesFileContent);
        } else {
            $this->info('Route comment handle not found.');
            return false;
        }

        file_put_contents($routesFile, $data);
        $this->info('Route <comment>'.$this->routeName().'</comment> added successfully.');
    }

    protected function generateRequest()
    {
        $requestName = $this->tableTitleCase() . 'Request';
        $requestFile = app_path('Http/Requests') . '/' . $requestName . '.php';

        if ($this->confirmOverwrite($requestFile)) {
            $content = view($this->templatesDir . '.request', [
                'request' => $requestName,
                'gen'     => $this
            ]);
            $this->createPhpFile($requestFile, $content);
            $this->info('<comment>'.$requestName.'</comment> generated successfully.');
        }
    }

    protected function generateBinding()
    {
        $binding = "\$router->model('{$this->routeName()}', 'App\\Models\\{$this->model}');";
        $providerFile = app_path('Providers/RouteServiceProvider.php');
        $providerFileContents = file_get_contents($providerFile);

        if (strpos($providerFileContents, $binding)) {
            $this->info('Binding already exists.');
            return false;
        }

        $bindingHandle = '/(public function boot\(Router \$router\)\s*\{)/';
        if (preg_match($bindingHandle, $providerFileContents)) {
            $data = preg_replace($bindingHandle, "$1\n\t\t{$binding}", $providerFileContents);
            file_put_contents($providerFile, $data);
            $this->info('Route binding for <comment>'.$this->routeName().'</comment> added successfully.');
        } else {
            $this->error('Route binding could not be added.');
        }
    }

    protected function generateController()
    {
        $controllerName = $this->tableTitleCase() . 'Controller';
        $requestName    = $this->tableTitleCase() . 'Request';
        $namespace      = strtolower($this->namespace);

        if ($this->paginate) {
            $indexMethod = 'orderBy(\'id\', \'DESC\')->paginate('.(int)$this->paginate.')';
        } elseif ($this->sortable) {
            $indexMethod = "ordenados()->get()";
        } else {
            $indexMethod = 'orderBy(\'id\', \'DESC\')->get()';
        }

        if ( ! file_exists(app_path('Http/Controllers/'.$this->namespace))) mkdir(app_path('Http/Controllers/'.$this->namespace), 0777, true);
        $controllerFile = app_path('Http/Controllers') . '/' . $this->namespace . '/' . $controllerName . '.php';

        if ($this->confirmOverwrite($controllerFile)) {
            $content = view($this->templatesDir . '.controller', [
                'gen'            => $this,
                'controllerName' => $controllerName,
                'requestName'    => $requestName,
                'namespace'      => $namespace,
                'route'          => $this->routeName(),
                'indexMethod'    => $indexMethod
            ]);
            $this->createPhpFile($controllerFile, $content);
            $this->info('Controller <comment>' . $this->tableTitleCase() .'Controller </comment> generated successfully');
        }
    }

    protected function generateViews()
    {
        $namespace = strtolower($this->namespace);
        $viewsDir  = base_path('resources/views/' . $namespace . '/' . $this->routeName());
        if ( ! file_exists($viewsDir)) mkdir($viewsDir, 0777, true);

        foreach (['index', 'create', 'edit', 'form'] as $view) {
            $viewFile = $viewsDir . '/' . $view . '.blade.php';
            if ($this->confirmOverwrite($viewFile)) {
                $content = view($this->templatesDir . '.' . $view, [
                    'gen'       => $this,
                    'namespace' => $namespace,
                    'route'     => $this->routeName()
                ]);
                $this->createViewFile($viewFile, $content);
                $this->info('<comment>'.ucfirst($view).'</comment> view generated successfully.');
            }
        }
    }

    protected function generateNav()
    {
        $namespace = strtolower($this->namespace);
        $nav = "<li @if(str_is('painel.{$this->routeName()}*', Route::currentRouteName())) class=\"active\" @endif>\n\t\t<a href=\"{{ route('{$namespace}.{$this->routeName()}.index') }}\">{$this->resourceName}</a>\n\t</li>";
        $navFile = base_path('resources/views/' . $namespace . '/common/nav.blade.php');
        $navFileContents = file_get_contents($navFile);

        if (strpos($navFileContents, $nav)) {
            $this->info('Navigation link already exists.');
            return false;
        }

        $navHandle = '/(<ul class="nav navbar-nav">)/';
        if (preg_match($navHandle, $navFileContents)) {
            $data = preg_replace($navHandle, "$1\n\t{$nav}", $navFileContents);
            file_put_contents($navFile, $data);
            $this->info('Navigation link for <comment>'.$this->routeName().'</comment> added successfully.');
        } else {
            $this->error('Navigation link could not be added.');
        }
    }
}
