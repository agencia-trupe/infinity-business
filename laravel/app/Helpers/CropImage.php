<?php

namespace App\Helpers;

use Request, Image;

class CropImage
{

    public static function make($input, $object)
    {
        if (!Request::hasFile($input) || !$object) return false;

        $image = Request::file($input);

        $name  = str_slug(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$image->getClientOriginalExtension();

        if (!is_array(array_values($object)[0])) $object = array($object);

        foreach($object as $config) {
            $width  = $config['width'];
            $height = $config['height'];
            $path   = $config['path'].$name;
            $upsize = (array_key_exists('upsize', $config) ? $config['upsize'] : false);
            $cliente = (array_key_exists('cliente', $config) ? $config['cliente'] : false);
            $bgcolor = (array_key_exists('bg', $config) ? $config['bg'] : false);

            if (!file_exists(public_path($config['path']))) {
                mkdir(public_path($config['path']), 0777, true);
            }

            $imgobj = Image::make($image->getRealPath());

            if ($width > 0 && $height > 0) {
                $ratio  = $imgobj->width() / $imgobj->height();
            }

            if ($width == null && $height == null) {
                $imgobj->save($path, 100);
            } elseif ($cliente) {
                $canvas = Image::canvas($width, $height, '#fff');
                $imagem = Image::make($imgobj)->resize($width, $height, function($constraint)
                {
                    $constraint->aspectRatio();
                });
                $imgobj = $canvas->insert($imagem, 'center')->save($path, 100);
            } elseif ($width == null || $height == null) {
                $imgobj->resize($width, $height, function($constraint) use ($upsize) {
                    $constraint->aspectRatio();
                    if ($upsize) { $constraint->upsize(); }
                })->save($path, 100);
            } elseif ($bgcolor && $ratio >= 1) {
                $imgobj->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas($width, $height, 'center', false, $bgcolor)->save($path, 100);
            } elseif ($bgcolor && $ratio < 1) {
                $imgobj->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->resizeCanvas($width, $height, 'center', false, $bgcolor)->save($path, 100);
            } else {
                $imgobj->fit($width, $height, function ($constraint) use ($upsize) {
                    if ($upsize) { $constraint->upsize(); }
                })->save($path, 100);
            }

            $imgobj->destroy();
        }

        return $name;
    }

}
