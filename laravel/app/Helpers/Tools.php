<?php

namespace App\Helpers;

class Tools
{

    public static function formataData($data, $locale)
    {
        $meses = [
            'pt' => [
                'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'
            ],
            'en' => [
                'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'
            ],
            'es' => [
                'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'
            ]
        ];

        list($mes, $ano) = explode('/', $data);

        return $meses[$locale][(int) $mes - 1] . ' ' . $ano;
    }

}
