<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('imoveisNaoLidos', \App\Models\ImovelRecebido::naoLidos()->count());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('solicitacoesNaoLidas', \App\Models\Solicitacao::naoLidas()->count());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('planos', \App\Models\Plano::orderBy('id', 'ASC')->get());
            $view->with('contato', \App\Models\Contato::first());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
