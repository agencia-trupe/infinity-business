<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('midia', 'App\Models\Midia');
		$router->model('imagens', 'App\Models\MidiaImagem');
		$router->model('video', 'App\Models\Video');
        $router->model('clientes', 'App\Models\Cliente');
		$router->model('historico', 'App\Models\Historico');
		$router->model('facilities', 'App\Models\Facilities');
		$router->model('comparativo', 'App\Models\Comparativo');
		$router->model('perfis', 'App\Models\Perfil');
        $router->model('fotos', 'App\Models\Foto');
		$router->model('planos', 'App\Models\Plano');
		$router->model('solicitacoes-de-test-drive', 'App\Models\Solicitacao');
		$router->model('indicacoes', 'App\Models\Indicacao');
		$router->model('servicos', 'App\Models\Servico');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('imoveis', 'App\Models\ImovelRecebido');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
