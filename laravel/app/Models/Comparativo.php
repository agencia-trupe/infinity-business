<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comparativo extends Model
{
    protected $table = 'comparativo';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function planos()
    {
        return $this->belongsToMany('App\Models\Plano', 'comparativo_planos', 'comparativo_id', 'plano_id')->withTimestamps();
    }
}
