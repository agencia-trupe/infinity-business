<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model
{
    protected $table = 'planos';

    protected $guarded = ['id'];

    public function getTipoAttribute($tipo)
    {
        return (int) $tipo;
    }
}
