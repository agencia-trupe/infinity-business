<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitacao extends Model
{
    protected $table = 'solicitacoes_de_test_drive';

    protected $guarded = ['id'];

    public function scopeNaoLidas($query)
    {
        return $query->where('lido', '!=', 1);
    }
}
