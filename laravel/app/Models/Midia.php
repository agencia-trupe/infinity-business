<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Midia extends Model
{
    protected $table = 'midia';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('m/Y', $date)->format('Y-m');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m', $date)->format('m/Y');
    }

    public function imagens()
    {
        return $this->hasMany('App\Models\MidiaImagem', 'midia_id')->ordenados();
    }

    public static function uploadPdf() {
        $file = \Request::file('pdf');

        $path = 'assets/pdfs/';
        $name = str_slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)).'_'.date('YmdHis').'.'.$file->getClientOriginalExtension();

        $file->move($path, $name);
        return $name;
    }
}
