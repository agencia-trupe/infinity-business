var config      = require('../config'),
    gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    plumber     = require('gulp-plumber');

gulp.task('vendor', function() {
    gulp.src(config.vendorPlugins)
        .pipe(plumber())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.build.js))
});
