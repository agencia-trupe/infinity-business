-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: infinitybusiness
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comparativo`
--

DROP TABLE IF EXISTS `comparativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comparativo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `comparativo` text COLLATE utf8_unicode_ci NOT NULL,
  `comparativo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comparativo`
--

LOCK TABLES `comparativo` WRITE;
/*!40000 ALTER TABLE `comparativo` DISABLE KEYS */;
INSERT INTO `comparativo` VALUES (1,0,'Endereço corporativo privilegiado','','2016-03-16 15:15:31','2016-03-18 00:12:03'),(2,2,'Acesso 24/7','','2016-03-16 15:15:40','2016-03-16 15:15:40'),(3,3,'Sala privada mobiliada e equipada','','2016-03-16 15:15:50','2016-03-16 15:15:50'),(4,4,'Sinalização Digital','','2016-03-16 15:15:58','2016-03-16 15:15:58'),(5,5,'Linha telefônica digital privada com caixa postal e acesso remoto','','2016-03-16 15:16:22','2016-03-18 00:13:06'),(6,6,'Equipamento telefônico digital exclusivo','','2016-03-17 16:19:00','2016-03-17 16:19:00'),(7,7,'Atendimento billíngüe personalizado','','2016-03-17 16:19:27','2016-03-17 16:19:27'),(8,8,'Redirecionamento de ligações','','2016-03-17 16:19:36','2016-03-17 16:19:36'),(10,10,'04 horas gratuitas de Salas de Reunião por mês','','2016-03-17 16:19:54','2016-03-17 16:19:54'),(11,11,'Acesso ao espaço Infinity Zen','','2016-03-17 16:20:03','2016-03-17 16:20:03'),(12,12,'Acesso a Rede de Relacionamento Infinity','','2016-03-17 16:20:14','2016-03-17 16:20:14'),(13,13,'Acesso a estrutura de Facilities do Edifício ','','2016-03-17 16:20:23','2016-03-17 16:20:23'),(14,14,'Acesso aos Eventos de Networking','','2016-03-17 16:20:30','2016-03-17 16:20:30'),(15,15,'Mailbox/ Caixa de Correspondências','','2016-03-17 16:20:36','2016-03-17 16:20:36'),(16,16,'Locker/ Armários Individuais*','','2016-03-17 16:20:45','2016-03-18 00:18:48'),(17,17,'Estacionamento com manobrista*','','2016-03-17 16:20:56','2016-03-18 00:18:58'),(18,18,'Digital Signage Plan*','','2016-03-17 16:21:08','2016-03-18 00:19:16'),(19,19,'Preços Especiais para Salas de Reunião/Showroom/ Treinamento/Auditório','','2016-03-17 16:21:19','2016-03-18 00:20:57'),(20,20,'Organização de Eventos*','','2016-03-17 16:21:32','2016-03-18 00:20:16'),(21,21,'Videoconferência*','','2016-03-17 16:21:42','2016-03-18 00:17:15'),(22,22,'Serviços gráficos*','','2016-03-17 16:21:50','2016-03-18 00:20:03'),(23,23,'Serviços de secretariado, concierge e mensageiro*','','2016-03-17 16:21:58','2016-03-22 19:00:15'),(25,25,'Projeto e Design de Interiores*','','2016-03-17 16:22:16','2016-03-18 00:16:21'),(26,26,'Estação de trabalho privada em sala compartilhada','','2016-03-17 16:22:30','2016-03-17 16:22:30'),(27,27,'Mesa de trabalho/ hot desk rotativa ','','2016-03-17 16:22:40','2016-03-17 16:22:40'),(28,1,'ISS Reduzido','','2016-03-18 00:11:24','2016-03-18 00:11:24');
/*!40000 ALTER TABLE `comparativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comparativo_planos`
--

DROP TABLE IF EXISTS `comparativo_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comparativo_planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comparativo_id` int(10) unsigned NOT NULL,
  `plano_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comparativo_planos_comparativo_id_foreign` (`comparativo_id`),
  KEY `comparativo_planos_plano_id_foreign` (`plano_id`),
  CONSTRAINT `comparativo_planos_comparativo_id_foreign` FOREIGN KEY (`comparativo_id`) REFERENCES `comparativo` (`id`),
  CONSTRAINT `comparativo_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comparativo_planos`
--

LOCK TABLES `comparativo_planos` WRITE;
/*!40000 ALTER TABLE `comparativo_planos` DISABLE KEYS */;
INSERT INTO `comparativo_planos` VALUES (1,1,1,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(2,1,2,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(3,1,3,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(4,1,4,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(5,2,1,'2016-03-16 15:15:40','2016-03-16 15:15:40'),(6,2,2,'2016-03-16 15:15:40','2016-03-16 15:15:40'),(7,3,1,'2016-03-16 15:15:50','2016-03-16 15:15:50'),(8,4,1,'2016-03-16 15:15:58','2016-03-16 15:15:58'),(9,5,1,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(10,5,2,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(12,5,4,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(13,6,1,'2016-03-17 16:19:00','2016-03-17 16:19:00'),(14,6,2,'2016-03-17 16:19:00','2016-03-17 16:19:00'),(15,7,1,'2016-03-17 16:19:27','2016-03-17 16:19:27'),(16,7,2,'2016-03-17 16:19:27','2016-03-17 16:19:27'),(18,7,4,'2016-03-17 16:19:27','2016-03-17 16:19:27'),(19,8,1,'2016-03-17 16:19:36','2016-03-17 16:19:36'),(20,8,2,'2016-03-17 16:19:36','2016-03-17 16:19:36'),(22,8,4,'2016-03-17 16:19:36','2016-03-17 16:19:36'),(27,10,1,'2016-03-17 16:19:54','2016-03-17 16:19:54'),(28,11,1,'2016-03-17 16:20:03','2016-03-17 16:20:03'),(29,11,2,'2016-03-17 16:20:03','2016-03-17 16:20:03'),(30,11,3,'2016-03-17 16:20:03','2016-03-17 16:20:03'),(31,11,4,'2016-03-17 16:20:03','2016-03-17 16:20:03'),(32,12,1,'2016-03-17 16:20:14','2016-03-17 16:20:14'),(33,12,2,'2016-03-17 16:20:14','2016-03-17 16:20:14'),(34,12,3,'2016-03-17 16:20:14','2016-03-17 16:20:14'),(35,12,4,'2016-03-17 16:20:14','2016-03-17 16:20:14'),(36,13,1,'2016-03-17 16:20:23','2016-03-17 16:20:23'),(37,13,2,'2016-03-17 16:20:23','2016-03-17 16:20:23'),(38,14,1,'2016-03-17 16:20:30','2016-03-17 16:20:30'),(39,14,2,'2016-03-17 16:20:30','2016-03-17 16:20:30'),(40,14,3,'2016-03-17 16:20:30','2016-03-17 16:20:30'),(41,14,4,'2016-03-17 16:20:30','2016-03-17 16:20:30'),(42,15,1,'2016-03-17 16:20:36','2016-03-17 16:20:36'),(43,15,2,'2016-03-17 16:20:36','2016-03-17 16:20:36'),(44,15,3,'2016-03-17 16:20:37','2016-03-17 16:20:37'),(45,15,4,'2016-03-17 16:20:37','2016-03-17 16:20:37'),(46,16,1,'2016-03-17 16:20:45','2016-03-17 16:20:45'),(47,16,2,'2016-03-17 16:20:45','2016-03-17 16:20:45'),(48,16,3,'2016-03-17 16:20:45','2016-03-17 16:20:45'),(49,16,4,'2016-03-17 16:20:45','2016-03-17 16:20:45'),(50,17,1,'2016-03-17 16:20:56','2016-03-17 16:20:56'),(51,17,2,'2016-03-17 16:20:56','2016-03-17 16:20:56'),(52,17,3,'2016-03-17 16:20:56','2016-03-17 16:20:56'),(53,17,4,'2016-03-17 16:20:56','2016-03-17 16:20:56'),(54,18,1,'2016-03-17 16:21:08','2016-03-17 16:21:08'),(55,18,2,'2016-03-17 16:21:08','2016-03-17 16:21:08'),(56,18,3,'2016-03-17 16:21:08','2016-03-17 16:21:08'),(57,18,4,'2016-03-17 16:21:08','2016-03-17 16:21:08'),(58,19,1,'2016-03-17 16:21:19','2016-03-17 16:21:19'),(59,19,2,'2016-03-17 16:21:19','2016-03-17 16:21:19'),(60,19,3,'2016-03-17 16:21:19','2016-03-17 16:21:19'),(61,19,4,'2016-03-17 16:21:19','2016-03-17 16:21:19'),(62,20,1,'2016-03-17 16:21:32','2016-03-17 16:21:32'),(63,20,2,'2016-03-17 16:21:32','2016-03-17 16:21:32'),(64,20,3,'2016-03-17 16:21:32','2016-03-17 16:21:32'),(65,20,4,'2016-03-17 16:21:32','2016-03-17 16:21:32'),(66,21,1,'2016-03-17 16:21:42','2016-03-17 16:21:42'),(67,21,2,'2016-03-17 16:21:42','2016-03-17 16:21:42'),(68,21,3,'2016-03-17 16:21:42','2016-03-17 16:21:42'),(69,21,4,'2016-03-17 16:21:42','2016-03-17 16:21:42'),(70,22,1,'2016-03-17 16:21:50','2016-03-17 16:21:50'),(71,22,2,'2016-03-17 16:21:50','2016-03-17 16:21:50'),(72,22,3,'2016-03-17 16:21:50','2016-03-17 16:21:50'),(73,22,4,'2016-03-17 16:21:50','2016-03-17 16:21:50'),(74,23,1,'2016-03-17 16:21:58','2016-03-17 16:21:58'),(75,23,2,'2016-03-17 16:21:58','2016-03-17 16:21:58'),(76,23,3,'2016-03-17 16:21:58','2016-03-17 16:21:58'),(77,23,4,'2016-03-17 16:21:58','2016-03-17 16:21:58'),(82,25,1,'2016-03-17 16:22:16','2016-03-17 16:22:16'),(83,25,2,'2016-03-17 16:22:16','2016-03-17 16:22:16'),(84,25,3,'2016-03-17 16:22:16','2016-03-17 16:22:16'),(85,25,4,'2016-03-17 16:22:16','2016-03-17 16:22:16'),(86,26,2,'2016-03-17 16:22:30','2016-03-17 16:22:30'),(87,27,3,'2016-03-17 16:22:40','2016-03-17 16:22:40'),(88,28,1,'2016-03-18 00:11:24','2016-03-18 00:11:24'),(89,28,2,'2016-03-18 00:11:24','2016-03-18 00:11:24'),(90,28,3,'2016-03-18 00:11:24','2016-03-18 00:11:24'),(91,28,4,'2016-03-18 00:11:24','2016-03-18 00:11:24');
/*!40000 ALTER TABLE `comparativo_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `codigo_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@infinitybusiness.com','11 3736·8600','<p>Alameda Rio Negro, 503 &middot; 23&ordm;&nbsp;andar</p>\r\n\r\n<p>Edif&iacute;cio Escrit&oacute;rios Rio Negro</p>\r\n\r\n<p>Alphaville &middot; Barueri, SP</p>\r\n\r\n<p>06454-000</p>\r\n','','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.8695065199336!2d-46.850324285023504!3d-23.50120928471281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf0223bf7d7ea3%3A0x5c69e5300371b547!2sAlameda+Rio+Negro%2C+503+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1457456321376\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,'2016-03-17 23:42:09');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
INSERT INTO `contatos_recebidos` VALUES (1,'tiago correa','tcorrea@nuclegem.com.br','26902606','Bom dia, \nGostaria de um orçamento para endereço fiscal e comercial. \nAtenciosamente,\nTiago correa \n',0,'2016-03-28 20:22:23','2016-03-28 20:22:23'),(2,'teste','teste@teste.com.br','111111111111','teste',0,'2016-03-29 00:18:19','2016-03-29 00:18:19');
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facilities`
--

DROP TABLE IF EXISTS `facilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facilities`
--

LOCK TABLES `facilities` WRITE;
/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` VALUES (1,'<p>A Infinity Business Network escolheu um edif&iacute;cio com o conceito inovador de <em>Facilities Office</em>,&nbsp; solu&ccedil;&otilde;es para tornar mais f&aacute;cil e prazeroso o dia-a-dia corporativo,&nbsp; tanto durante o expediente como nas horas livres.</p>\r\n\r\n<p>Sua localiza&ccedil;&atilde;o &eacute; privilegiada, estando na principal via de Alphaville, Alameda Rio Negro,&nbsp;em uma regi&atilde;o com o ISS reduzido e&nbsp;ao lado de importantes rodovias como a Castelo Branco e o Rodoanel. Nosso Business Center est&aacute;&nbsp;cercado&nbsp;de shoppings centers, ao lado do Iguatemi, diversos&nbsp;restaurantes, hot&eacute;is e com&eacute;rcio sofisticado.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','','<p>Servi&ccedil;os b&aacute;sicos e em sistema de&nbsp;<em>pay-per-use&nbsp;</em>de concierge, limpeza, courier,&nbsp;administra&ccedil;&atilde;o de eventos, academia, vesti&aacute;rios com chuveiros,&nbsp;quadra de t&ecirc;nis, sal&atilde;o de jogos, sal&atilde;o de beleza, caf&eacute; e restaurante e controle de acesso&nbsp;s&atilde;o algumas das facilidades do edif&iacute;cio que escolhemos estar,&nbsp;visando atender com seguran&ccedil;a e tecnologia as mais diversas necessidades,&nbsp;contribuindo diretamente para a melhora da produtividade de seus usu&aacute;rios.</p>\r\n\r\n<p>Qualidade |&nbsp;Conveni&ecirc;ncia |&nbsp;Lucratividade&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','',NULL,'2016-03-18 00:01:46');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci,
  `galeria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (18,0,'IMG_9175_20160324194953.jpg',NULL,'experimente','2016-03-24 22:49:53','2016-03-24 22:49:53'),(19,0,'IMG_9264_20160324195005.jpg',NULL,'experimente','2016-03-24 22:50:06','2016-03-24 22:50:06'),(20,0,'IMG_9283_20160324195015.jpg',NULL,'experimente','2016-03-24 22:50:15','2016-03-24 22:50:15'),(21,0,'IMG_9305_20160324195027.jpg',NULL,'experimente','2016-03-24 22:50:28','2016-03-24 22:50:28'),(22,0,'IMG_9314_20160324195051.jpg',NULL,'contato','2016-03-24 22:50:52','2016-03-24 22:50:52'),(23,0,'IMG_9242_20160324195104.jpg',NULL,'contato','2016-03-24 22:51:04','2016-03-24 22:51:04'),(24,0,'IMG_9068_20160324195115.jpg',NULL,'contato','2016-03-24 22:51:16','2016-03-24 22:51:16'),(25,0,'IMG_9229_20160324195130.jpg',NULL,'contato','2016-03-24 22:51:30','2016-03-24 22:51:30'),(26,0,'IMG_9132_20160324195136.jpg',NULL,'contato','2016-03-24 22:51:36','2016-03-24 22:51:36'),(27,0,'IMG_8962_20160324200456.jpg',NULL,'conceito','2016-03-24 23:04:57','2016-03-24 23:04:57'),(28,0,'IMG_9051_20160324200506.jpg',NULL,'conceito','2016-03-24 23:05:06','2016-03-24 23:05:06'),(29,0,'IMG_9096_20160324200525.jpg',NULL,'conceito','2016-03-24 23:05:25','2016-03-24 23:05:25'),(30,0,'IMG_9086_20160324200537.jpg',NULL,'conceito','2016-03-24 23:05:38','2016-03-24 23:05:38');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historico`
--

DROP TABLE IF EXISTS `historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historico`
--

LOCK TABLES `historico` WRITE;
/*!40000 ALTER TABLE `historico` DISABLE KEYS */;
INSERT INTO `historico` VALUES (1,'<p>Respons&aacute;vel por trazer ao Brasil o conceito de escrit&oacute;rios inteligentes em 1992, o s&oacute;cio-diretor, Mauro Koraicho, criou uma s&oacute;lida empresa com 17 filiais e mais de 15 mil clientes&nbsp;que administrou durante 18 anos.</p>\r\n\r\n<p>Agora, aproveitando o avan&ccedil;o tecnol&oacute;gico dos &uacute;ltimos anos, a Infinity est&aacute; de volta com um novo centro de neg&oacute;cios em cerca de 1500 m2 no rec&eacute;m-inaugurado Empresarial Escrit&oacute;rios Rio Negro, em Alphaville.</p>\r\n\r\n<p>Os clientes Infinity contam com um conceito aprimorado de escrit&oacute;rios prontos, identidade empresarial e eventos de neg&oacute;cios, onde todas as facilidades estruturais e de servi&ccedil;os necess&aacute;rios no dia a dia corporativo est&atilde;o presentes num ambiente de alto padr&atilde;o, preocupado tamb&eacute;m com o bem-estar e o desenvolvimento do <em>networking</em> de cada usu&aacute;rio.</p>\r\n\r\n<p>A Networking Community desenvolvida pela Infinity objetiva integrar as pessoas certas, que podem se ajudar profissionalmente, dar refer&ecirc;ncias e fazer boas indica&ccedil;&otilde;es, atrav&eacute;s da realiza&ccedil;&atilde;o de eventos e <em>happy-hours </em>internos.</p>\r\n\r\n<p>Nosso moderno e flex&iacute;vel espa&ccedil;o contribui diretamente para a melhoria da performance profissional de seus usu&aacute;rios, j&aacute; que n&atilde;o h&aacute; preocupa&ccedil;&otilde;es com o funcionamento operacional, apenas foco integral no trabalho e no relacionamento.</p>\r\n\r\n<p>Tudo isso ao lado do Shopping Iguatemi Alphaville na Alameda Rio Negro, a principal via regi&atilde;o, localizada a apenas 15&nbsp;minutos da cidade de S&atilde;o Paulo e no centro dos tr&ecirc;s maiores aeroportos da regi&atilde;o: Congonhas, Guarulhos e Viracopos.</p>\r\n','',NULL,'2016-03-18 00:32:54');
/*!40000 ALTER TABLE `historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicacoes`
--

DROP TABLE IF EXISTS `indicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `indicado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `por` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicacoes`
--

LOCK TABLES `indicacoes` WRITE;
/*!40000 ALTER TABLE `indicacoes` DISABLE KEYS */;
INSERT INTO `indicacoes` VALUES (1,'teste@teste.com.br','teste@teste.com.br','2016-03-29 00:23:48','2016-03-29 00:23:48');
/*!40000 ALTER TABLE `indicacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_03_01_170622_create_servicos_table',1),('2016_03_02_034737_create_indicacoes_table',1),('2016_03_02_040119_create_solicitacoes_de_test_drive_table',1),('2016_03_02_043142_create_planos_table',1),('2016_03_02_185620_create_fotos_table',1),('2016_03_02_195530_create_perfis_table',1),('2016_03_02_195913_create_perfis_planos_table',1),('2016_03_02_204736_create_comparativo_table',1),('2016_03_02_204756_create_comparativo_planos_table',1),('2016_03_04_222323_create_facilities_table',1),('2016_03_07_165736_create_historico_table',1),('2016_03_10_020656_create_video_table',1),('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_03_01_170622_create_servicos_table',1),('2016_03_02_034737_create_indicacoes_table',1),('2016_03_02_040119_create_solicitacoes_de_test_drive_table',1),('2016_03_02_043142_create_planos_table',1),('2016_03_02_185620_create_fotos_table',1),('2016_03_02_195530_create_perfis_table',1),('2016_03_02_195913_create_perfis_planos_table',1),('2016_03_02_204736_create_comparativo_table',1),('2016_03_02_204756_create_comparativo_planos_table',1),('2016_03_04_222323_create_facilities_table',1),('2016_03_07_165736_create_historico_table',1),('2016_03_10_020656_create_video_table',1),('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_03_01_170622_create_servicos_table',1),('2016_03_02_034737_create_indicacoes_table',1),('2016_03_02_040119_create_solicitacoes_de_test_drive_table',1),('2016_03_02_043142_create_planos_table',1),('2016_03_02_185620_create_fotos_table',1),('2016_03_02_195530_create_perfis_table',1),('2016_03_02_195913_create_perfis_planos_table',1),('2016_03_02_204736_create_comparativo_table',1),('2016_03_02_204756_create_comparativo_planos_table',1),('2016_03_04_222323_create_facilities_table',1),('2016_03_07_165736_create_historico_table',1),('2016_03_10_020656_create_video_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,0,'Startup','','startup','<p>Comece seu neg&oacute;cio dentro de um ambiente estruturado, que passe seguran&ccedil;a para seus clientes e fornecedores&nbsp;e que propicie&nbsp;relacionamentos profissionais.&nbsp;</p>\r\n','','2016-03-16 15:10:37','2016-03-21 19:39:09'),(2,1,'Pequena ou Média Empresa','','pequena-ou-media-empresa','<p>Estrutura completa de espa&ccedil;o, servi&ccedil;os e tecnologia capaz de&nbsp;otimizar a opera&ccedil;&atilde;o da sua empresa, contribuindo&nbsp;com a produtividade dos funcion&aacute;rios e potencializando&nbsp;as chances de crescimento no mercado.</p>\r\n','','2016-03-16 15:10:44','2016-03-21 19:40:21'),(3,2,'Empresa de Grande Porte','','empresa-de-grande-porte','<p>Servi&ccedil;os de qualidade em espa&ccedil;o corporativo completo para opera&ccedil;&atilde;o&nbsp;em tempo integral, projetos especiais, reestrutura&ccedil;&otilde;es (<em>downsizing</em> e expans&otilde;es), reuni&otilde;es, treinamentos e eventos.&nbsp;</p>\r\n','','2016-03-16 15:10:51','2016-03-21 19:42:58'),(4,3,'ONG','','ong','<p>Ganhe for&ccedil;a para atrair investimentos em local profissional estruturado para reuni&otilde;es internas, recep&ccedil;&atilde;o de convidados e atividades do cotidiano em ambiente 100%&nbsp;corporativo e bem localizado.</p>\r\n','','2016-03-16 15:10:56','2016-03-21 19:48:17'),(5,4,'Profissional Autônomo','','profissional-autonomo','<p>Ambiente profissional&nbsp;completo, com infraestrutura&nbsp;e servi&ccedil;os de alta qualidade sob demanda, para facilitar as atividades do dia-a-dia e otimizar a sua imagem corporativa.</p>\r\n','','2016-03-16 15:11:03','2016-03-21 19:50:55'),(6,5,'Home-Office','','home-office','<p>Troque a informalidade caseira por&nbsp;um espa&ccedil;o dedicado a profissionais que visam maior produtividade e destaque&nbsp;no mercado, contando diariamente com uma&nbsp;estrutura corporativa&nbsp;completa para trabalhar e servi&ccedil;os de qualidade&nbsp;sob demanda para realizar reuni&otilde;es e eventos.</p>\r\n','','2016-03-16 15:11:08','2016-03-21 19:54:15');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis_planos`
--

DROP TABLE IF EXISTS `perfis_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis_planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `perfil_id` int(10) unsigned NOT NULL,
  `plano_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perfis_planos_perfil_id_foreign` (`perfil_id`),
  KEY `perfis_planos_plano_id_foreign` (`plano_id`),
  CONSTRAINT `perfis_planos_perfil_id_foreign` FOREIGN KEY (`perfil_id`) REFERENCES `perfis` (`id`),
  CONSTRAINT `perfis_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis_planos`
--

LOCK TABLES `perfis_planos` WRITE;
/*!40000 ALTER TABLE `perfis_planos` DISABLE KEYS */;
INSERT INTO `perfis_planos` VALUES (2,1,2,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(3,1,3,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(4,1,4,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(5,1,5,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(7,1,7,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(8,2,1,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(12,2,5,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(14,2,7,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(15,3,1,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(19,3,5,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(21,3,7,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(23,4,2,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(24,4,3,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(26,4,5,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(28,4,7,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(29,5,1,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(30,5,2,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(31,5,3,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(33,5,5,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(35,5,7,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(37,6,2,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(38,6,3,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(40,6,5,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(42,6,7,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(43,1,6,'2016-03-21 19:38:35','2016-03-21 19:38:35'),(44,2,6,'2016-03-21 19:40:21','2016-03-21 19:40:21'),(45,3,6,'2016-03-21 19:42:27','2016-03-21 19:42:27'),(46,4,4,'2016-03-21 19:48:17','2016-03-21 19:48:17'),(47,4,6,'2016-03-21 19:48:17','2016-03-21 19:48:17'),(48,5,4,'2016-03-21 19:50:55','2016-03-21 19:50:55'),(49,5,6,'2016-03-21 19:50:55','2016-03-21 19:50:55'),(50,6,4,'2016-03-21 19:51:53','2016-03-21 19:51:53'),(51,6,6,'2016-03-21 19:51:53','2016-03-21 19:51:53');
/*!40000 ALTER TABLE `perfis_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planos`
--

DROP TABLE IF EXISTS `planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sigla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planos`
--

LOCK TABLES `planos` WRITE;
/*!40000 ALTER TABLE `planos` DISABLE KEYS */;
INSERT INTO `planos` VALUES (1,'Premium Business Plan','','premium-business-plan','PBP',1,'1.500,00','Escritórios prontos exclusivos, equipados com móveis de alto padrão, tecnologia de ponta e serviços personalizados. Contratos de locação flexíveis e sem investimento inicial.','','<p>Salas privativas de 11 a 50 m2 com acesso 24/7 e&nbsp;esta&ccedil;&otilde;es de trabalho completas (mesa, cadeira, gaveteiro, internet, linha e aparelho telef&ocirc;nico digital com atendimento personalizado, redirecionamento de liga&ccedil;&otilde;es&nbsp;e caixa postal com acesso remoto).&nbsp;&Aacute;rea comum (Infinity + Condom&iacute;nio) com mais de 1500 m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ banheiros/ copas/ dep&oacute;sitos/ academia/ vesti&aacute;rio/ quadra de t&ecirc;nis/ sala de jogos/ restaurante/ sal&atilde;o de beleza).&nbsp;Servi&ccedil;os de copa, limpeza, e manuten&ccedil;&atilde;o interna de TI (rede e telefonia) e gerenciamento de escrit&oacute;rio.&nbsp;Servi&ccedil;os sob demanda de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;Acesso aos eventos de networking oferecidos pela Infinity (palestras/ cursos/ happy hours, etc.) e a rede de relacionamento corporativo da Infinity. 04&nbsp;horas gratuitas por m&ecirc;s de sala de reuni&atilde;o com TV para at&eacute; 07 pessoas sujeito &agrave; disponibilidade.&nbsp;Pre&ccedil;os especiais para loca&ccedil;&atilde;o de espa&ccedil;os de reuni&otilde;es, treinamentos, showroom e eventos empresariais.</p>\r\n','',NULL,'2016-03-17 22:52:20'),(2,'Shared Business Plan','','shared-business-plan','SBP',1,'1.400,00','Estações de trabalho privativas completas em ambiente executivo compartilhado com acesso 24 horas / 7 dias na semana.','','<p>Sala fechada (cart&atilde;o de acesso individual) com&nbsp;esta&ccedil;&otilde;es de trabalho completas (mesa, cadeira, gaveteiro&nbsp;e aparelho telef&ocirc;nico digital com senha e caixa postal individuais) e arm&aacute;rios sob demanda.&nbsp;Linha telef&ocirc;nica exclusiva com atendimento personalizado, caixa postal com acesso remoto e&nbsp;transfer&ecirc;ncia de liga&ccedil;&otilde;es.&nbsp;Uso compartilhado da &aacute;rea comum (Infinity + Condom&iacute;nio) com cerca de 1500m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ banheiros/ copas/ dep&oacute;sitos/ academia/ vesti&aacute;rio/ quadra de t&ecirc;nis/ sala de jogos/ restaurante/ sal&atilde;o de beleza); Acesso a servi&ccedil;os copa, limpeza, manuten&ccedil;&atilde;o de TI (rede e telefonia), gerenciamento de escrit&oacute;rio e, sob demanda, de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;Acesso aos eventos de Networking oferecidos pela Infinity (palestras/ cursos/ happy hours, etc.) e a rede de relacionamento corporativo da Infinity.</p>\r\n','',NULL,'2016-03-17 22:48:15'),(3,'Coworking Business Plan','','coworking-business-plan','CBP',1,'1.000,00','Estações de trabalho rotativas em ambiente executivo compartilhado com acesso livre em dias úteis, das 9h às 18h.','','<p>Ambiente aberto com&nbsp;posi&ccedil;&otilde;es de trabalho localizadas na &aacute;rea comum da Infinity.&nbsp;Hot desking com mesas, poltronas e acesso &agrave; rede wi-fi&nbsp;para uso sempre que necess&aacute;rio. Possibilidade de contrata&ccedil;&atilde;o de servi&ccedil;os extras (salas de reuni&otilde;es, treinamento, showroom e eventos/ secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos/ estacionamento com manobrista/ etc.) sempre que necess&aacute;rio, mediante&nbsp;disponibilidade.</p>\r\n','',NULL,'2016-03-17 22:49:07'),(4,'Virtual Business Plan','','virtual-business-plan','VBP',1,'400,00','Endereço empresarial privilegiado com linha telefônica exclusiva e atendimento bilíngue personalizado. Escritórios e salas de reuniões à disposição sob demanda. Acesso livre das 9h às 18h em dias úteis.','','<p>Endere&ccedil;o corporativo de prest&iacute;gio, com ISS reduzido, para fins comerciais e fiscais.&nbsp;Linha telef&ocirc;nica privada com atendimento bil&iacute;ngue personalizado, caixa postal com acesso remoto e redirecionamento de liga&ccedil;&otilde;es. Caixa de correspond&ecirc;ncia exclusiva.&nbsp;Acesso em hor&aacute;rio comercial a &aacute;rea comum da Infinity com cerca de 500 m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ audit&oacute;rio/ banheiros/ copas/ dep&oacute;sitos/salas de reuni&atilde;o, treinamento e showroom).&nbsp;Acesso a servi&ccedil;os sob demanda de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;</p>\r\n','',NULL,'2016-03-17 22:54:00'),(5,'Digital Signage Plan','','digital-signage-plan','',2,'','Produção e divulgação de vídeos, propagandas, apresentações e animações corporativas reproduzidas nos monitores e videowall do centro de negócio. Inclusão da marca no diretório Infinity.  INVESTIMENTOS SOB CONSULTA.','','<p>Apresenta&ccedil;&otilde;es, v&iacute;deos e anima&ccedil;&otilde;es corporativas personalizadas reproduzidas para rede de network da Infinity Business atrav&eacute;s do diret&oacute;rio interno, nos&nbsp;monitores e&nbsp;videowall&nbsp;do centro de neg&oacute;cio. Parceria com empresa de produ&ccedil;&atilde;o de material digital.</p>\r\n','',NULL,'2016-03-17 23:22:35'),(6,'Networking','','networking','',2,'','Acesso à rede de relacionamento Infinity.','','<p>Ingresso na comunidade&nbsp;Infinity, atrav&eacute;s de eventos peri&oacute;dicos com conte&uacute;do de qualidade e happy-hours que objetivam ampliar a rede de relacionamento profissional dos participantes.</p>\r\n','',NULL,'2016-03-22 18:37:31'),(7,'Eventos e Reuniões','','eventos-e-reunioes','',2,'','Modernas e equipadas salas de reuniões, auditórios e espaços para conferências, showrooms, workshops e treinamentos com serviços de coffee-break e videoconferência.','','<p><strong>Or&ccedil;amentos personalizados.&nbsp;A PARTIR DE R$ 50,00/HORA.</strong></p>\r\n','',NULL,'2016-03-22 18:38:21');
/*!40000 ALTER TABLE `planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,0,'Acesso 24/7','','2016-03-16 15:17:53','2016-03-16 15:17:53'),(2,1,'Contratos Flexíveis','','2016-03-16 15:18:01','2016-03-22 18:04:40'),(3,9,'Solicitações Sob Demanda','','2016-03-16 15:18:11','2016-03-22 18:57:15'),(4,11,'Equipe Bilíngue','','2016-03-16 15:18:17','2016-03-22 18:13:55'),(5,17,'Transferência de Ligações','','2016-03-16 15:18:24','2016-03-16 15:18:24'),(6,12,'Atendimento Personalizado','','2016-03-16 15:18:32','2016-03-16 15:18:32'),(7,13,'Secretariado/Courier/Concierge','','2016-03-17 16:12:50','2016-03-22 17:56:04'),(8,5,'Fatura Única Mensal','','2016-03-17 16:12:56','2016-03-22 17:56:50'),(9,6,'Administração de Espaço e Pessoas','','2016-03-17 16:13:09','2016-03-22 18:05:00'),(10,14,'Caixa-Postal com Acesso Remoto','','2016-03-17 16:13:18','2016-03-22 17:56:28'),(11,16,'Administração de Correspondências','','2016-03-17 16:13:30','2016-03-22 18:08:51'),(12,18,'Serviço de Copa e Limpeza','','2016-03-17 16:13:40','2016-03-22 17:57:21'),(13,21,'Espaço de Descompressão','','2016-03-17 16:13:52','2016-03-22 18:06:20'),(14,2,'Sem Fiador ou Seguro Fiança','','2016-03-17 16:14:00','2016-03-22 18:02:09'),(15,19,'Área de Copa e Café','','2016-03-17 16:14:15','2016-03-22 18:10:00'),(16,22,'Serviços Gráficos ','','2016-03-17 16:14:25','2016-03-17 16:14:25'),(17,24,'Auditórios com Projeção','','2016-03-17 16:14:42','2016-03-17 16:14:42'),(18,23,'Espaços Equipados para Reuniões ','','2016-03-17 16:14:54','2016-03-22 18:07:04'),(19,26,'Coffee-break ','','2016-03-17 16:15:11','2016-03-17 16:15:11'),(20,25,'Videoconferência  ','','2016-03-17 16:15:22','2016-03-17 16:15:22'),(21,27,'Lockers Individuais','','2016-03-17 16:15:38','2016-03-17 16:15:38'),(23,15,'Mail Box Privado','','2016-03-22 18:09:02','2016-03-22 18:50:02'),(24,20,'Vending Machines','','2016-03-22 18:10:06','2016-03-22 18:10:06'),(25,7,'Telefonia Digital','','2016-03-22 18:12:40','2016-03-22 18:12:40'),(26,8,'Tecnologia de Ponta','','2016-03-22 18:12:58','2016-03-22 18:12:58'),(27,3,'Início Imediato','','2016-03-22 18:25:20','2016-03-22 18:25:20'),(28,4,'Endereço com ISS Reduzido','','2016-03-22 18:51:48','2016-03-22 18:51:48'),(29,10,'Estacionamento e Segurança','','2016-03-22 18:56:57','2016-03-22 18:56:57');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacoes_de_test_drive`
--

DROP TABLE IF EXISTS `solicitacoes_de_test_drive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitacoes_de_test_drive` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estacao_de_trabalho` tinyint(1) NOT NULL DEFAULT '0',
  `sala_de_reuniao` tinyint(1) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitacoes_de_test_drive`
--

LOCK TABLES `solicitacoes_de_test_drive` WRITE;
/*!40000 ALTER TABLE `solicitacoes_de_test_drive` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacoes_de_test_drive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$6rIQCrsu0aHQYH9kE5xO4eZl7njKVz/c93QsYlDwQFK8a7r/bNKuu','nnJnI65twNns8ryCuWUf0HG21iij5LYnRb1WLkL3jyOZeJ5b4VxeMup6rii6',NULL,'2016-03-17 16:46:39'),(2,'infinity','contato@infinitybysiness.com.br','$2y$10$7oyH8NKcgd18SpTcOv/hM.wO7TbJW.Sr68Jbn7nVdlW.P.MHK.4XG','YCp2orHWCHp0HY46WphDLq3tDo6d7Hw0rp79tA8BTgSd9bYGbco211o4Kpdo','2016-03-17 16:46:32','2016-03-24 23:05:44');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (1,'bg-infinitybusiness_20160317132954.jpg','youtube','wVSyq77EmJ8',NULL,'2016-03-17 16:29:55');
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-30 14:33:04
