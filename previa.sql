-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: infinitybusiness
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comparativo`
--

DROP TABLE IF EXISTS `comparativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comparativo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `comparativo` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comparativo`
--

LOCK TABLES `comparativo` WRITE;
/*!40000 ALTER TABLE `comparativo` DISABLE KEYS */;
INSERT INTO `comparativo` VALUES (1,0,'Endereço corporativo','2016-03-16 15:15:31','2016-03-16 15:15:31'),(2,0,'Acesso 24/7','2016-03-16 15:15:40','2016-03-16 15:15:40'),(3,0,'Sala privada mobiliada e equipada','2016-03-16 15:15:50','2016-03-16 15:15:50'),(4,0,'Sinalização Digital','2016-03-16 15:15:58','2016-03-16 15:15:58'),(5,0,'Linha telefônica ditial privada com caixa postal','2016-03-16 15:16:22','2016-03-16 15:16:22');
/*!40000 ALTER TABLE `comparativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comparativo_planos`
--

DROP TABLE IF EXISTS `comparativo_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comparativo_planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comparativo_id` int(10) unsigned NOT NULL,
  `plano_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comparativo_planos_comparativo_id_foreign` (`comparativo_id`),
  KEY `comparativo_planos_plano_id_foreign` (`plano_id`),
  CONSTRAINT `comparativo_planos_comparativo_id_foreign` FOREIGN KEY (`comparativo_id`) REFERENCES `comparativo` (`id`),
  CONSTRAINT `comparativo_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comparativo_planos`
--

LOCK TABLES `comparativo_planos` WRITE;
/*!40000 ALTER TABLE `comparativo_planos` DISABLE KEYS */;
INSERT INTO `comparativo_planos` VALUES (1,1,1,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(2,1,2,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(3,1,3,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(4,1,4,'2016-03-16 15:15:31','2016-03-16 15:15:31'),(5,2,1,'2016-03-16 15:15:40','2016-03-16 15:15:40'),(6,2,2,'2016-03-16 15:15:40','2016-03-16 15:15:40'),(7,3,1,'2016-03-16 15:15:50','2016-03-16 15:15:50'),(8,4,1,'2016-03-16 15:15:58','2016-03-16 15:15:58'),(9,5,1,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(10,5,2,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(11,5,3,'2016-03-16 15:16:23','2016-03-16 15:16:23'),(12,5,4,'2016-03-16 15:16:23','2016-03-16 15:16:23');
/*!40000 ALTER TABLE `comparativo_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `codigo_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@infinitybusiness.com','11 3736·1800','<p>Alameda Rio Negro, 503 &middot; 23&ordm;&nbsp;andar</p>\r\n\r\n<p>Edif&iacute;cio Rio Negro</p>\r\n\r\n<p>Alphaville &middot; Barueri, SP</p>\r\n\r\n<p>06454-000</p>\r\n','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.8695065199336!2d-46.850324285023504!3d-23.50120928471281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf0223bf7d7ea3%3A0x5c69e5300371b547!2sAlameda+Rio+Negro%2C+503+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1457456321376\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,'2016-03-16 15:17:14');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facilities`
--

DROP TABLE IF EXISTS `facilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facilities`
--

LOCK TABLES `facilities` WRITE;
/*!40000 ALTER TABLE `facilities` DISABLE KEYS */;
INSERT INTO `facilities` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempus lacinia auctor. Integer aliquam diam vel elit finibus rutrum. Pellentesque mollis finibus pulvinar. Etiam et maximus lorem, et posuere risus. Curabitur vitae aliquam ipsum. Pellentesque euismod rutrum sapien, sit amet ultrices lacus tempor ut. Phasellus aliquet ipsum a aliquam convallis. Aliquam erat volutpat. Praesent laoreet libero eu nibh congue pulvinar. Suspendisse eu ligula eu sem rutrum aliquet vel nec odio. Curabitur vel imperdiet dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In mattis pellentesque efficitur. Donec gravida nisi a interdum varius. Praesent placerat accumsan ultricies. Donec ullamcorper sed nisi sit amet ultrices.</p>\r\n','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:10:02');
/*!40000 ALTER TABLE `facilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotos`
--

DROP TABLE IF EXISTS `fotos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci,
  `galeria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotos`
--

LOCK TABLES `fotos` WRITE;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` VALUES (1,0,'1_20160316152050.jpg',NULL,'conceito','2016-03-16 15:20:51','2016-03-16 15:20:51'),(2,1,'2_20160316152051.jpg',NULL,'conceito','2016-03-16 15:20:51','2016-03-16 15:20:51'),(3,2,'1_20160316152055.jpg',NULL,'conceito','2016-03-16 15:20:55','2016-03-16 15:20:55'),(4,3,'2_20160316152056.jpg',NULL,'conceito','2016-03-16 15:20:56','2016-03-16 15:20:56'),(5,0,'1_20160316152118.jpg',NULL,'experimente','2016-03-16 15:21:18','2016-03-16 15:21:18'),(6,1,'2_20160316152118.jpg',NULL,'experimente','2016-03-16 15:21:19','2016-03-16 15:21:19'),(7,2,'1_20160316152122.jpg',NULL,'experimente','2016-03-16 15:21:23','2016-03-16 15:21:23'),(8,3,'2_20160316152123.jpg',NULL,'experimente','2016-03-16 15:21:23','2016-03-16 15:21:23'),(9,0,'1_20160316152145.jpg',NULL,'contato','2016-03-16 15:21:46','2016-03-16 15:21:46'),(10,3,'2_20160316152146.jpg',NULL,'contato','2016-03-16 15:21:46','2016-03-16 15:21:46'),(11,2,'1_20160316152157.jpg',NULL,'contato','2016-03-16 15:21:57','2016-03-16 15:21:57'),(12,1,'2_20160316152157.jpg',NULL,'contato','2016-03-16 15:21:57','2016-03-16 15:21:57'),(13,4,'1_20160316152201.jpg',NULL,'contato','2016-03-16 15:22:01','2016-03-16 15:22:01');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historico`
--

DROP TABLE IF EXISTS `historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historico`
--

LOCK TABLES `historico` WRITE;
/*!40000 ALTER TABLE `historico` DISABLE KEYS */;
INSERT INTO `historico` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempus lacinia auctor. Integer aliquam diam vel elit finibus rutrum. Pellentesque mollis finibus pulvinar. Etiam et maximus lorem, et posuere risus. Curabitur vitae aliquam ipsum. Pellentesque euismod rutrum sapien, sit amet ultrices lacus tempor ut. Phasellus aliquet ipsum a aliquam convallis. Aliquam erat volutpat. Praesent laoreet libero eu nibh congue pulvinar. Suspendisse eu ligula eu sem rutrum aliquet vel nec odio. Curabitur vel imperdiet dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. In mattis pellentesque efficitur. Donec gravida nisi a interdum varius. Praesent placerat accumsan ultricies. Donec ullamcorper sed nisi sit amet ultrices.</p>\r\n\r\n<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:16:50');
/*!40000 ALTER TABLE `historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `indicacoes`
--

DROP TABLE IF EXISTS `indicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `indicacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `indicado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `por` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `indicacoes`
--

LOCK TABLES `indicacoes` WRITE;
/*!40000 ALTER TABLE `indicacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `indicacoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_03_01_170622_create_servicos_table',1),('2016_03_02_034737_create_indicacoes_table',1),('2016_03_02_040119_create_solicitacoes_de_test_drive_table',1),('2016_03_02_043142_create_planos_table',1),('2016_03_02_185620_create_fotos_table',1),('2016_03_02_195530_create_perfis_table',1),('2016_03_02_195913_create_perfis_planos_table',1),('2016_03_02_204736_create_comparativo_table',1),('2016_03_02_204756_create_comparativo_planos_table',1),('2016_03_04_222323_create_facilities_table',1),('2016_03_07_165736_create_historico_table',1),('2016_03_10_020656_create_video_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis`
--

DROP TABLE IF EXISTS `perfis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis`
--

LOCK TABLES `perfis` WRITE;
/*!40000 ALTER TABLE `perfis` DISABLE KEYS */;
INSERT INTO `perfis` VALUES (1,0,'Startup','startup','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:10:37','2016-03-16 15:10:37'),(2,1,'Pequena ou Média Empresa','pequena-ou-media-empresa','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:10:44','2016-03-16 15:10:44'),(3,2,'Empresa de Grande Porte','empresa-de-grande-porte','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:10:51','2016-03-16 15:10:51'),(4,3,'ONG','ong','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:10:56','2016-03-16 15:10:56'),(5,4,'Profissional Autônomo','profissional-autonomo','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:11:03','2016-03-16 15:11:03'),(6,5,'Home-Office','home-office','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis. Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante. Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat. Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo. Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n','2016-03-16 15:11:08','2016-03-16 15:11:08');
/*!40000 ALTER TABLE `perfis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfis_planos`
--

DROP TABLE IF EXISTS `perfis_planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfis_planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `perfil_id` int(10) unsigned NOT NULL,
  `plano_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perfis_planos_perfil_id_foreign` (`perfil_id`),
  KEY `perfis_planos_plano_id_foreign` (`plano_id`),
  CONSTRAINT `perfis_planos_perfil_id_foreign` FOREIGN KEY (`perfil_id`) REFERENCES `perfis` (`id`),
  CONSTRAINT `perfis_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfis_planos`
--

LOCK TABLES `perfis_planos` WRITE;
/*!40000 ALTER TABLE `perfis_planos` DISABLE KEYS */;
INSERT INTO `perfis_planos` VALUES (1,1,1,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(2,1,2,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(3,1,3,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(4,1,4,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(5,1,5,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(6,1,6,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(7,1,7,'2016-03-16 15:10:37','2016-03-16 15:10:37'),(8,2,1,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(9,2,2,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(10,2,3,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(11,2,4,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(12,2,5,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(13,2,6,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(14,2,7,'2016-03-16 15:10:44','2016-03-16 15:10:44'),(15,3,1,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(16,3,2,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(17,3,3,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(18,3,4,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(19,3,5,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(20,3,6,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(21,3,7,'2016-03-16 15:10:51','2016-03-16 15:10:51'),(22,4,1,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(23,4,2,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(24,4,3,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(25,4,4,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(26,4,5,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(27,4,6,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(28,4,7,'2016-03-16 15:10:56','2016-03-16 15:10:56'),(29,5,1,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(30,5,2,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(31,5,3,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(32,5,4,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(33,5,5,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(34,5,6,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(35,5,7,'2016-03-16 15:11:03','2016-03-16 15:11:03'),(36,6,1,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(37,6,2,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(38,6,3,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(39,6,4,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(40,6,5,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(41,6,6,'2016-03-16 15:11:08','2016-03-16 15:11:08'),(42,6,7,'2016-03-16 15:11:08','2016-03-16 15:11:08');
/*!40000 ALTER TABLE `perfis_planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planos`
--

DROP TABLE IF EXISTS `planos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sigla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planos`
--

LOCK TABLES `planos` WRITE;
/*!40000 ALTER TABLE `planos` DISABLE KEYS */;
INSERT INTO `planos` VALUES (1,'Premium Business Plan','premium-business-plan','PBP',1,'1.500,00','Escritórios prontos exclusivos, equipados com móveis de alto padrão, tecnologia de ponta e serviços personalizados. Contratos de locação flexíveis e sem investimento inicial.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:12:55'),(2,'Shared Business Plan','shared-business-plan','SBP',1,'1.250,00','Estações de trabalho privativas completas em ambiente executivo compartilhado com acesso 24 horas / 7 dias na semana.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:12:58'),(3,'Coworking Business Plan','coworking-business-plan','CBP',1,'1.000,00','Estações de trabalho rotativas em ambiente executivo compartilhado com acesso livre em dias úteis, das 9h às 18h.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:14:04'),(4,'Virtual Business Plan','virtual-business-plan','VBP',1,'400,00','Endereço empresarial privilegiado com linha telefônica exclusiva e atendimento bilíngue personalizado. Escritórios e salas de reuniões à disposição sob demanda. Acesso livre das 9h às 18h em dias úteis.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:13:03'),(5,'Digital Signage Plan','digital-signage-plan','',2,'','Produção e divulgação de vídeos e animações corporativas personalizadas reproduzidas para rede de network da Infinity Business e nos monitores e videowalls do centro de negócio. INVESTIMENTOS SOB CONSULTA.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:13:53'),(6,'Networking','networking','',2,'','Participação na rede de networking da Infinity, através de rede de relacionamento privada on-line, eventos periódicos com conteúdo de qualidade e happy-hours que objetivam ampliar a rede de relacionamento profissional dos participantes.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:14:10'),(7,'Eventos e Reuniões','eventos-e-reunioes','',2,'','Modernas e equipadas salas de reuniões, auditórios e espaços para conferências, showrooms, workshops e treinamentos com serviços de coffee-break e videoconferência.','<p>Cras pretium augue non efficitur consequat. Sed ut ligula vel augue elementum dictum non at purus. Aenean id sem eu magna accumsan laoreet. Praesent nec nisi nec urna ornare gravida. Suspendisse ut facilisis velit. Nunc sagittis pretium augue. Suspendisse congue laoreet dui vel convallis.</p>\r\n\r\n<ul>\r\n	<li>Phasellus dolor nibh, mollis vel libero nec, pellentesque tempor ante.</li>\r\n	<li>Suspendisse venenatis urna ut nisi accumsan, a consectetur diam volutpat.</li>\r\n	<li>Sed congue, eros sit amet interdum gravida, metus lacus condimentum lectus, sit amet aliquet odio felis at justo.</li>\r\n</ul>\r\n\r\n<p>Nunc faucibus bibendum nisi in posuere. Nulla urna felis, bibendum sed convallis at, placerat in turpis. Sed sed maximus metus. Donec faucibus felis ut gravida euismod.</p>\r\n',NULL,'2016-03-16 15:14:13');
/*!40000 ALTER TABLE `planos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,0,'Acesso 24/7','2016-03-16 15:17:53','2016-03-16 15:17:53'),(2,1,'Administração e Gerenciamento','2016-03-16 15:18:01','2016-03-16 15:18:01'),(3,2,'Recepcionista Bilíngue','2016-03-16 15:18:11','2016-03-16 15:18:11'),(4,3,'Telefonista Bilíngue','2016-03-16 15:18:17','2016-03-16 15:18:17'),(5,4,'Transferência de Ligações','2016-03-16 15:18:24','2016-03-16 15:18:24'),(6,5,'Atendimento Personalizado','2016-03-16 15:18:32','2016-03-16 15:18:32');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitacoes_de_test_drive`
--

DROP TABLE IF EXISTS `solicitacoes_de_test_drive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitacoes_de_test_drive` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estacao_de_trabalho` tinyint(1) NOT NULL DEFAULT '0',
  `sala_de_reuniao` tinyint(1) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitacoes_de_test_drive`
--

LOCK TABLES `solicitacoes_de_test_drive` WRITE;
/*!40000 ALTER TABLE `solicitacoes_de_test_drive` DISABLE KEYS */;
/*!40000 ALTER TABLE `solicitacoes_de_test_drive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$6rIQCrsu0aHQYH9kE5xO4eZl7njKVz/c93QsYlDwQFK8a7r/bNKuu',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `video`
--

DROP TABLE IF EXISTS `video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `video`
--

LOCK TABLES `video` WRITE;
/*!40000 ALTER TABLE `video` DISABLE KEYS */;
INSERT INTO `video` VALUES (1,'','youtube','wVSyq77EmJ8',NULL,'2016-03-16 15:09:24');
/*!40000 ALTER TABLE `video` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-16 15:26:14
